﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;

namespace ABWS.Controllers.V1
{
    [Microsoft.Web.Http.ApiVersion("1.0")]

    public class CostCenterController : ApiController
    {
        [Route("api/v{version:apiVersion}/costcenter")]
        //  List all cost centers by user.
        //  GET: api/v1/costcenter
        public HttpResponseMessage Get()
        {
            HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
            var method = System.Reflection.MethodBase.GetCurrentMethod().Name;
            var url = this.Url.Request.RequestUri.AbsolutePath;

            string exceptionMsg = null;
            string userId = "0";

            try
            {
                var db = new DBAccess.DBDataAccess();

                userId = Utils.Configuration.GetUserId(User);

                //  Get the Cost Center List by server user.
                var costCenterMDList = db.GetCCByUserId(userId);

                // Convert the Cost Center List to csv format.
                var retVal = Utils.CSVFormat.GetCostCenterMD(costCenterMDList);

                result.Content = new StringContent(retVal);
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
            }
            catch (Exception ex)
            {
                result = new HttpResponseMessage(HttpStatusCode.InternalServerError);
                exceptionMsg = ex.Message;
            }
            finally
            {
                Utils.WSLogHelper.AddWSLog(userId, method, url, (int)result.StatusCode, exceptionMsg);
            }

            return result;
        }

        [Route("api/v{version:apiVersion}/costcenter/{id}")]
        //  Get the information about a Cost Center.
        //  GET: api/v1/costcenter/1/
        public HttpResponseMessage Get(string id)
        {
            HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
            var method = System.Reflection.MethodBase.GetCurrentMethod().Name;
            var url = this.Url.Request.RequestUri.AbsolutePath;

            string exceptionMsg = null;
            string userId = "0";

            try
            {
                var db = new DBAccess.DBDataAccess();

                userId = Utils.Configuration.GetUserId(User);

                if (!db.CheckUserByCC(id, userId))
                    result = new HttpResponseMessage(HttpStatusCode.Unauthorized);
                else
                {
                    //  Get the Cost Center List by cost center code.
                    var costCenterMDList = db.GetCCByCCCode(id);

                    // Convert the Cost Center List to csv format.
                    var retVal = Utils.CSVFormat.GetCostCenterMD(costCenterMDList);

                    result = new HttpResponseMessage(HttpStatusCode.OK);
                    result.Content = new StringContent(retVal);
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                }
            }
            catch (Exception ex)
            {
                result = new HttpResponseMessage(HttpStatusCode.InternalServerError);
                exceptionMsg = ex.Message;
            }
            finally
            {
                Utils.WSLogHelper.AddWSLog(userId, method, url, (int)result.StatusCode, exceptionMsg);
            }
            return result;
        }
    }
}