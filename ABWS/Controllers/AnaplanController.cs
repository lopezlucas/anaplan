﻿using ABWS.BusinessLogic;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Hosting;
using System.Threading.Tasks;
using System.Threading;

namespace ABWS.Controllers.V1
{
    [Microsoft.Web.Http.ApiVersion("1.0")]
    public class AnaplanController : ApiController
    {
        [Route("api/v{version:apiVersion}/anaplan/{id}")]
        // POST: api/v1/anaplan/1/
        public HttpResponseMessage Post(string id)
        {
            HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.NoContent);
            var method = System.Reflection.MethodBase.GetCurrentMethod().Name;
            var url = this.Url.Request.RequestUri.AbsolutePath;

            string exceptionMsg = null;
            string userId = "0";

            try
            {
                var anaplanBL = new AnaplanBL();
                var db = new DBAccess.DBDataAccess();

                userId = Utils.Configuration.GetConfiguration("userAnaplan");

                //  Get token the last version by cost center.
                var token = db.GetTokenLastVersionByCCCode(id);
                //  Upload Financial Data and Headcount Data by cost center code.
                var isOk = anaplanBL.UploadFinacialDataAndHeadcountData(id, token);

                if (!isOk)
                {
                    //  New attempt for upload data.
                    HostingEnvironment.QueueBackgroundWorkItem(ct => uploadAnaplanAsync(ct, id));
                }
                else
                {
                    result = new HttpResponseMessage(HttpStatusCode.NoContent);
                }
            }
            catch (Exception ex)
            {
                result = new HttpResponseMessage(HttpStatusCode.InternalServerError);
                exceptionMsg = ex.Message + " " + ex.InnerException.ToString();
            }
            finally
            {
                Utils.WSLogHelper.AddWSLog(userId, method, url, (int)result.StatusCode, exceptionMsg);
            }
            return result;
        }

        [Route("api/v{version:apiVersion}/anaplan/")]
        // PUT: api/v1/anaplan/
        public string Put()
        {
            HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
            var method = System.Reflection.MethodBase.GetCurrentMethod().Name;
            var url = this.Url.Request.RequestUri.AbsolutePath;
            
            string exceptionMsg = null;
            string userId = "0";
            
            try
            {
                var anaplanBL = new AnaplanBL();
                //  Anaplan User.
                userId = Utils.Configuration.GetConfiguration("userAnaplan");
                //  Generate the new token with the server user y current date.
                var tokenVersion = Utils.Token.GenerateToken(userId);
                //  Send to anaplan the cost center data.
                var res = anaplanBL.DownloadFinacialDataAndHeadcountData(userId, tokenVersion);
            }
            catch (Exception ex)
            {
                result = new HttpResponseMessage(HttpStatusCode.InternalServerError);
                exceptionMsg = ex.Message;
            }
            finally
            {
                Utils.WSLogHelper.AddWSLog(userId, method, url, (int)result.StatusCode, exceptionMsg);
            }
            return "OK";
        }

        /// <summary>
        /// Task for upload Anaplan.
        /// </summary>
        /// <param name="ct"></param>
        /// <param name="cc_code"></param>
        /// <returns></returns>
        private async Task<CancellationToken> uploadAnaplanAsync(CancellationToken ct, string cc_code)
        {
            var anaplanBL = new AnaplanBL();
            HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
            var method = System.Reflection.MethodBase.GetCurrentMethod().Name;
            var userId = "0";
            var isOk = true;

            try
            {
                //  Number of attempts for send to Anaplan.
                int attempts = Utils.Configuration.GetIntConfiguration("attemptsAnaplan");
                //  Time of wait for new send.
                int wait = Utils.Configuration.GetIntConfiguration("waitAnaplan");

                userId = Utils.Configuration.GetConfiguration("userAnaplan");

                for (int i = 0; i < attempts; i++)
                {
                    if (ct.IsCancellationRequested)
                    {
                        break;
                    }
                    var db = new DBAccess.DBDataAccess();

                    //  Get token the last version by cost center.
                    var token = db.GetTokenLastVersionByCCCode(cc_code);

                    //  If check not exist, send to Anaplan.
                    if (!db.CheckAnaplanTimestamp(token))
                    {
                        isOk = anaplanBL.UploadFinacialDataAndHeadcountData(cc_code, token);
                    }
                    else // If check exist not send and exit.
                    {
                        break;
                    }

                    if (isOk)
                    {
                        break;
                    }

                    // "Simulate" this operation took a long time, but was able to run without
                    // blocking the calling thread (i.e., it's doing I/O operations which are async)
                    // We use Task.Delay rather than Thread.Sleep, because Task.Delay returns
                    // the thread immediately back to the thread-pool, whereas Thread.Sleep blocks it.
                    // Task.Delay is essentially the asynchronous version of Thread.Sleep:
                    await Task.Delay(wait, ct);
                }
                
            }
            catch (TaskCanceledException tce)
            {
                //Trace.TraceError("Caught TaskCanceledException - signaled cancellation " + tce.Message);
                Utils.WSLogHelper.AddWSLog(userId, method, "", (int)result.StatusCode, "Caught TaskCanceledException - signaled cancellation " + tce.Message);
            }
            return ct;
        }
    }
}