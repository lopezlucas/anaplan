﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;

namespace ABWS.Controllers.V1
{
    [Microsoft.Web.Http.ApiVersion("1.0")]
    public class FinancialController : ApiController
    {
        [Route("api/v{version:apiVersion}/costcenter/{id}/financial")]
        //  Get last version financial entries associated to a cost center.
        //  GET: api/v1/costcenter/1/financial
        public HttpResponseMessage Get(string id)
        {
            HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
            var method = System.Reflection.MethodBase.GetCurrentMethod().Name;
            var url = this.Url.Request.RequestUri.AbsolutePath;

            string exceptionMsg = null;
            string userId = "0";

            try
            {
                var db = new DBAccess.DBDataAccess();

                userId = Utils.Configuration.GetUserId(User);

                if (!db.CheckUserByCC(id, userId))
                    result = new HttpResponseMessage(HttpStatusCode.Unauthorized);
                else
                {
                    //  Get token the last version by cost center.
                    var token = db.GetTokenLastVersionByCCCode(id);

                    var financialDataLst = db.GetVersionFDByCCCode(id,token);

                    // Convert the Cost Center List to csv format.
                    var retVal = Utils.CSVFormat.GetFinancialData(financialDataLst);

                    result = new HttpResponseMessage(HttpStatusCode.OK);
                    result.Content = new StringContent(retVal);
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                }
            }
            catch (Exception ex)
            {
                result = new HttpResponseMessage(HttpStatusCode.InternalServerError);
                exceptionMsg = ex.Message;
            }
            finally
            {
                Utils.WSLogHelper.AddWSLog(userId, method,url, (int)result.StatusCode, exceptionMsg);
            }
            return result;
        }

        [Route("api/v{version:apiVersion}/costcenter/{id}/financial/{timestamp}")]
        //  Get a specific version financial entries associated to a cost center.
        //  GET: api/v1/CostCenter/1/financial/timestamp
        public HttpResponseMessage Get(string id, string timestamp)
        {
            HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
            var method = System.Reflection.MethodBase.GetCurrentMethod().Name;
            var url = this.Url.Request.RequestUri.AbsolutePath;

            string exceptionMsg = null;
            string userId = "0";

            try
            {
                var db = new DBAccess.DBDataAccess();

                userId = Utils.Configuration.GetUserId(User);

                if (!db.CheckUserByCC(id, userId))
                    result = new HttpResponseMessage(HttpStatusCode.Unauthorized);
                else
                {
                    //  Get the Financial Data List by cost center and token.
                    var financialDataLst = db.GetVersionFDByCCCode(id, timestamp);

                    // Convert the Financial Data List to csv format.
                    var retVal = Utils.CSVFormat.GetFinancialData(financialDataLst);

                    result = new HttpResponseMessage(HttpStatusCode.OK);
                    result.Content = new StringContent(retVal);
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                }
            }
            catch (Exception ex)
            {
                result = new HttpResponseMessage(HttpStatusCode.InternalServerError);
                exceptionMsg = ex.Message;
            }
            finally
            {
                Utils.WSLogHelper.AddWSLog(userId, method, url, (int)result.StatusCode, exceptionMsg);
            }
            return result;
        }

        [Route("api/v{version:apiVersion}/costcenter/{id}/financial/{timestamp}")]
        //  Upload financial entries.
        //  POST: api/v1/costcenter/1/financial/timestamp
        public HttpResponseMessage Post(string id,string timestamp)
        {
            HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
            var method = System.Reflection.MethodBase.GetCurrentMethod().Name;
            var url = this.Url.Request.RequestUri.AbsolutePath;

            string exceptionMsg = null;
            string userId = "0";

            try
            {
                var db = new DBAccess.DBDataAccess();

                userId = Utils.Configuration.GetUserId(User);
                
                if (!db.CheckUserByCC(id, userId))
                    result = new HttpResponseMessage(HttpStatusCode.Unauthorized);
                else
                {
                    if (!db.ExistVersionFD(timestamp))
                    {
                        //  Get data for Upload Financial entries in format csv.
                        var body = this.Request.Content.ReadAsStringAsync().Result;

                        //  Convert the Financial Data entries to Financial Data List.
                        var financialDataLst = Utils.CSVFormat.GetFinancialDataListFromText(body, userId);

                        db.InsertFinancialData(financialDataLst, timestamp);

                        //  If exist version of Headcount Data update last change Cost Center.
                        if (db.ExistVersionHCD(timestamp))
                            db.UpdateLastChangeCostCenter(id, userId, DateTime.Now);
                    }
                    else
                    {
                        //  Indicate that exist insert Financial Data for the token.
                        result = new HttpResponseMessage(HttpStatusCode.NoContent);
                    }
                }
            }
            catch (Exception ex)
            {
                result = new HttpResponseMessage(HttpStatusCode.InternalServerError);
                exceptionMsg = ex.Message;
            }
            finally
            {
                Utils.WSLogHelper.AddWSLog(userId, method,url, (int)result.StatusCode, exceptionMsg);
            }
            return result;
        }
    }
}