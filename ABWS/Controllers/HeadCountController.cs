﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;

namespace ABWS.Controllers.V1
{
    [Microsoft.Web.Http.ApiVersion("1.0")]
    public class HeadCountController : ApiController
    {
        [Route("api/v{version:apiVersion}/costcenter/{id}/headcount")]
        //  Get last version headcount entries associated to a cost center.
        //  GET: api/v1/costcenter/1/headcount
        public HttpResponseMessage Get(string id)
        {
            HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
            var method = System.Reflection.MethodBase.GetCurrentMethod().Name;
            var url = this.Url.Request.RequestUri.AbsolutePath;

            string exceptionMsg = null;
            string userId = "0";

            try
            {
                var db = new DBAccess.DBDataAccess();

                userId = Utils.Configuration.GetUserId(User);

                if (!db.CheckUserByCC(id, userId))
                    result = new HttpResponseMessage(HttpStatusCode.Unauthorized);
                else
                {
                    //  Get token the last version by cost center.
                    var token = db.GetTokenLastVersionByCCCode(id);

                    //  Get the Headcount Data List by cost center and token.
                    var headcountDataLst = db.GetVersionHCByCCCode(id, token);

                    // Convert the Headcount Data List to csv format.
                    var retVal = Utils.CSVFormat.GetHeadCountData(headcountDataLst);

                    result = new HttpResponseMessage(HttpStatusCode.OK);
                    result.Content = new StringContent(retVal);
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                }
            }
            catch (Exception ex)
            {
                result = new HttpResponseMessage(HttpStatusCode.InternalServerError);
                exceptionMsg = ex.Message;
            }
            finally
            {
                Utils.WSLogHelper.AddWSLog(userId, method, url, (int)result.StatusCode, exceptionMsg);
            }
            return result;
        }

        [Route("api/v{version:apiVersion}/costcenter/{id}/headcount/{timestamp}")]
        //  Get a specific version headcount entries associated to a cost center.
        //  GET: api/v1/costcenter/1/headcount/timestamp
        public HttpResponseMessage Get(string id, string timestamp)
        {
            HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
            var method = System.Reflection.MethodBase.GetCurrentMethod().Name;
            var url = this.Url.Request.RequestUri.AbsolutePath;

            string exceptionMsg = null;
            string userId = "0";

            try
            {
                var db = new DBAccess.DBDataAccess();

                userId = Utils.Configuration.GetUserId(User);

                if (!db.CheckUserByCC(id, userId))
                    result = new HttpResponseMessage(HttpStatusCode.Unauthorized);
                else
                {
                    //  Get the Headcount Data List by server user.
                    var headcountDataLst = db.GetVersionHCByCCCode(id, timestamp);

                    // Convert the Headcount Data List to csv format.
                    var retVal = Utils.CSVFormat.GetHeadCountData(headcountDataLst);

                    result = new HttpResponseMessage(HttpStatusCode.OK);
                    result.Content = new StringContent(retVal);
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                }
            }
            catch (Exception ex)
            {
                result = new HttpResponseMessage(HttpStatusCode.InternalServerError);
                exceptionMsg = ex.Message;
            }
            finally
            {
                Utils.WSLogHelper.AddWSLog(userId, method, url, (int)result.StatusCode, exceptionMsg);
            }
            return result;
        }

        [Route("api/v{version:apiVersion}/costcenter/{id}/headcount/{timestamp}")]
        //  Upload headcoud entries.
        //  POST: api/v1/costcenter/1/headcount/{timestamp}
        public HttpResponseMessage Post(string id, string timestamp)
        {
            HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
            var method = System.Reflection.MethodBase.GetCurrentMethod().Name;
            var url = this.Url.Request.RequestUri.AbsolutePath;

            string exceptionMsg = null;
            string userId = "0";

            try
            {
                var db = new DBAccess.DBDataAccess();

                userId = Utils.Configuration.GetUserId(User);

                if (!db.CheckUserByCC(id, userId))
                    result = new HttpResponseMessage(HttpStatusCode.Unauthorized);
                else
                {
                    if (!db.ExistVersionHCD(timestamp))
                    {
                        //  Get data for Upload Headcount entries in format csv.
                        var body = this.Request.Content.ReadAsStringAsync().Result;

                        //  Convert the Headcount Data entries to Headcount Data List.
                        var headcountDataLst = Utils.CSVFormat.GetHeadCountDataListFromText(body, userId);

                        db.InsertHeadCountData(headcountDataLst, timestamp);

                        //  If exist version of Finacial Data update last change Cost Center.
                        if (db.ExistVersionFD(timestamp))
                            db.UpdateLastChangeCostCenter(id, userId, DateTime.Now);
                    }
                    else
                    {
                        //  Indicate that exist insert Headcount Data for the token.
                        result = new HttpResponseMessage(HttpStatusCode.NoContent);
                    }
                }

            }
            catch (Exception ex)
            {
                result = new HttpResponseMessage(HttpStatusCode.InternalServerError);
                exceptionMsg = ex.Message;
            }
            finally
            {
                Utils.WSLogHelper.AddWSLog(userId, method, url, (int)result.StatusCode, exceptionMsg);
            }
            return result;
        }
    }
}
