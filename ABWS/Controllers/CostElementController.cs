﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web.Http;

namespace ABWS.Controllers.V1
{
    [Microsoft.Web.Http.ApiVersion("1.0")]

    public class CostElementController : ApiController
    {
        [Route("api/v{version:apiVersion}/costcenter/{id}/costelement")]
        //  List of Cost Elements can be added to a cost center. 
        //  GET: api/v1/costcenter/1/costelement
        public HttpResponseMessage Get(string id)
        {
            HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
            var method = System.Reflection.MethodBase.GetCurrentMethod().Name;
            var url = this.Url.Request.RequestUri.AbsolutePath;
            var sb = new StringBuilder();
            
            string exceptionMsg = null;
            string userId = "0";

            try
            {
                var db = new DBAccess.DBDataAccess();

                userId = Utils.Configuration.GetUserId(User);

                if (!db.CheckUserByCC(id, userId))
                    result = new HttpResponseMessage(HttpStatusCode.Unauthorized);
                else
                {
                    //  Get the Cost Element List by cost center code
                    var costElementMDList = db.GetCEByCCCode(id);

                    // Convert the Cost Center List to csv format
                    var retVal = Utils.CSVFormat.GetCostElementMD(costElementMDList);

                    result = new HttpResponseMessage(HttpStatusCode.OK);
                    result.Content = new StringContent(retVal);
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                }
            }
            catch (Exception ex)
            {
                result = new HttpResponseMessage(HttpStatusCode.InternalServerError);
                exceptionMsg = ex.Message;
            }
            finally
            {
                Utils.WSLogHelper.AddWSLog(userId, method, url, (int)result.StatusCode, exceptionMsg);
            }
            return result;
        }

        [Route("api/v{version:apiVersion}/costcenter/{id}/costelement")]
        //  Add Cost Elemements to the cost center. 
        //  POST: api/v1/costcenter/1/costelement
        public HttpResponseMessage Post(string id)
        {
            HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
            var method = System.Reflection.MethodBase.GetCurrentMethod().Name;
            var url = this.Url.Request.RequestUri.AbsolutePath;

            string exceptionMsg = null;
            string userId = "0";

            try
            {
                var db = new DBAccess.DBDataAccess();

                userId = Utils.Configuration.GetUserId(User);

                if (!db.CheckUserByCC(id, userId))
                    result = new HttpResponseMessage(HttpStatusCode.Unauthorized);
                else
                {
                    //  Get data for Upload CostElement entries.
                    var body = this.Request.Content.ReadAsStringAsync().Result;

                    //  Generate new Token
                    var newToken = Utils.Token.GenerateToken(userId);

                    //  Get token of the last version by cost center.
                    var oldToken = db.GetTokenLastVersionByCCCode(id);

                    var version = new Models.Version(id, newToken, userId, DateTime.Now);

                    db.InsertVersion(version);

                    //  Get the Financial Data List by cost center and the old token. 
                    //  Set user ID for the last Financial Data.
                    var financialDataLst = db.GetVersionFDByCCCode(id, oldToken, userId);

                    //  Get the new rows for the insert Financial Data.
                    //  The new rows set the new token and current cost center.
                    var financialDataWithNewCostElementLst = Utils.CSVFormat.GetFinancialDataWithNewCostElementListFromText(body, id, newToken, financialDataLst[0]);

                    //  Add to the old list Financial Data the new list Financial Data for each Cost Element.
                    financialDataLst.AddRange(financialDataWithNewCostElementLst);

                    db.InsertFinancialData(financialDataLst, newToken);

                    //  Get the Headcount Data List by cost center and the old token. 
                    //  Set user ID for the last Headcount Data.
                    var headcountDataLst = db.GetVersionHCByCCCode(id, oldToken, userId);

                    db.InsertHeadCountData(headcountDataLst, newToken);
                }
            }
            catch (Exception ex)
            {
                result = new HttpResponseMessage(HttpStatusCode.InternalServerError);
                exceptionMsg = ex.Message;
            }
            finally
            {
                Utils.WSLogHelper.AddWSLog(userId, method, url, (int)result.StatusCode, exceptionMsg);
            }
            return result;
        }
    }
}
