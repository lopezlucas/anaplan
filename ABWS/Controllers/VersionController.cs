﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;

namespace ABWS.Controllers.V1
{
    [Microsoft.Web.Http.ApiVersion("1.0")]
    public class VersionController : ApiController
    {
        [Route("api/v{version:apiVersion}/costcenter/{id}/version")]
        //  Get the list of versions associated to a cost center.
        //  GET: api/v1/costcenter/1/version
        public HttpResponseMessage Get(string id)
        {
            HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
            var method = System.Reflection.MethodBase.GetCurrentMethod().Name;
            var url = this.Url.Request.RequestUri.AbsolutePath;

            string exceptionMsg = null;
            string userId = "0";

            try
            {
                var db = new DBAccess.DBDataAccess();

                userId = Utils.Configuration.GetUserId(User);

                if (!db.CheckUserByCC(id, userId))
                    result = new HttpResponseMessage(HttpStatusCode.Unauthorized);
                else
                {
                    //  Get the Cost Center version List by cost center.
                    var versionCostCenterList = db.GetVersionCCByCCCode(id);

                    // Convert the Cost Center version List to csv format.
                    var retVal = Utils.CSVFormat.GetVersionCostCenter(versionCostCenterList);

                    result = new HttpResponseMessage(HttpStatusCode.OK);
                    result.Content = new StringContent(retVal);
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                }
            }
            catch (Exception ex)
            {
                result = new HttpResponseMessage(HttpStatusCode.InternalServerError);
                exceptionMsg = ex.Message;
            }
            finally
            {
                Utils.WSLogHelper.AddWSLog(userId, method,url, (int)result.StatusCode, exceptionMsg);
            }
            return result;
        }

        [Route("api/v{version:apiVersion}/costcenter/{id}/token")]
        //  Get the new token version for the cost center.
        //  POST: api/v1/costcenter/1/token
        public HttpResponseMessage Post(string id)
        {
            HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
            var method = System.Reflection.MethodBase.GetCurrentMethod().Name;
            var url = this.Url.Request.RequestUri.AbsolutePath;

            string exceptionMsg = null;
            string userId = "0";

            try
            {
                var db = new DBAccess.DBDataAccess();

                userId = Utils.Configuration.GetUserId(User);

                //  Generate the new token with the server user y current date.
                var token = Utils.Token.GenerateToken(userId);

                var version = new Models.Version(id, token, userId, DateTime.Now);

                db.InsertVersion(version);

                result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new StringContent(token);
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");

            }
            catch (Exception ex)
            {
                result = new HttpResponseMessage(HttpStatusCode.InternalServerError);
                exceptionMsg = ex.Message;
            }
            finally
            {
                Utils.WSLogHelper.AddWSLog(userId, method, url, (int)result.StatusCode, exceptionMsg);
            }
            return result;
        }

        [Route("api/v{version:apiVersion}/plugin/version")]
        //  Get the plugin version.
        //  GET: api/v1/plugin/version
        public HttpResponseMessage Get()
        {
            //  Get the api version of the web config.
            string apiVersion = Utils.Configuration.GetConfiguration("apiVersion");
            
            var result = new HttpResponseMessage(HttpStatusCode.OK);
            result.Content = new StringContent(apiVersion);
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");

            return result;
        }

    }
}