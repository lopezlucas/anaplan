﻿using System.Web.Http;

namespace ABWS
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API routes
            /*Se comentó para poder utilizar el versionado de de las rutas.
            Esto se configura en el Global.asax*/
            //config.MapHttpAttributeRoutes();
            // Other Web API configuration not shown.
        }
    }
}
