﻿using System;
using ABWS.Models;

namespace ABWS.Utils
{
    public class WSLogHelper
    {
        public const string MESSAGE_OK = "The method executed correctly.";
        public const string MESSAGE_ERROR = "An error occurred while executing the method.";
        public const string MESSAGE_UNAUTHORIZED = "The user is not authorized.";
        public const string MESSAGE_ERROR_EX = "An error occurred while executing the method.Returns the following exception : ";

        //  Indicates that the request was successful.
        public const int STATUS_OK = 200;
        //  Indicates that a generic error occurred on the server.
        public const int STATUS_INTERNAL_SERVER_ERROR = 500;
        //  Indicates that the requested resource does not exist on the server.
        public const int STATUS_NOT_FOUND = 404;
        //  Indicates that the requested resource requires authentication.
        public const int STATUS_UNAUTHORIZED = 401;

        /// <summary>
        /// Add web server log according to http status code.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="method"></param>
        /// <param name="url"></param>
        /// <param name="httpStatusCode"></param>
        /// <param name="ex"></param>
        public static void AddWSLog(string userId, string method, string url, int httpStatusCode, string ex)
        {
            DBAccess.DBDataAccess db = new DBAccess.DBDataAccess();
            WSLog wsLog = new WSLog();
            var serverMsg = "";

            //  validate status code.
            switch (httpStatusCode)
            {
                case STATUS_OK:
                    serverMsg = MESSAGE_OK;
                    break;
                case STATUS_INTERNAL_SERVER_ERROR:
                    serverMsg = MESSAGE_ERROR_EX + ex;
                    break;
                case STATUS_NOT_FOUND:
                    serverMsg = MESSAGE_ERROR;
                    break;
                case STATUS_UNAUTHORIZED:
                    serverMsg = MESSAGE_UNAUTHORIZED;
                    break;
                default:
                    break;
            }

            //  Add parameters to log.
            wsLog.UserId = userId;
            wsLog.Method = method;
            wsLog.Url = url;
            wsLog.StatusCode = httpStatusCode;
            wsLog.WebServerMessage = serverMsg;
            //  Set the server date.
            wsLog.AccessDate = DateTime.Now;

            db.InsertWSLog(wsLog);
        }
    }
}