﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace ABWS.Utils
{
    public class Token
    {
        /// <summary>
        /// Generate the token with the user ID and the current date.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static string GenerateToken(string userId)
        {
            var str = DateTime.Now + userId;

            MD5 md5 = MD5CryptoServiceProvider.Create();
            ASCIIEncoding encoding = new ASCIIEncoding();
            byte[] stream = null;
            StringBuilder sb = new StringBuilder();

            stream = md5.ComputeHash(encoding.GetBytes(str));

            for (int i = 0; i < stream.Length; i++)
            {
                sb.AppendFormat("{0:x2}", stream[i]);
            }

            return sb.ToString();
        }
    }
}