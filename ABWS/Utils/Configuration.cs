﻿using System.Configuration;
using System.Net.Http;
using System.Text;

namespace ABWS.Utils
{
    public class Configuration
    {
        /// <summary>
        /// Get the current configuration url base to connect Anaplan.
        /// </summary>
        /// <returns></returns>
        public static string GetUrlBaseAnaPlan()
        {
            var sbUrlBase = new StringBuilder();

            //  Get the url Anaplan.
            sbUrlBase.Append(GetConfiguration("apiAnaPlan"));
            sbUrlBase.Append(GetConfiguration("majorVersionID"));
            sbUrlBase.Append("/");
            sbUrlBase.Append(GetConfiguration("minorVersionID"));
            sbUrlBase.Append("/workspaces/");
            sbUrlBase.Append(GetConfiguration("workspaceID"));
            sbUrlBase.Append("/models/");
            sbUrlBase.Append(GetConfiguration("modelID"));

            return sbUrlBase.ToString();
        }

        /// <summary>
        /// Get the url connect to Anaplan for the import data with the specific chunk.
        /// </summary>
        /// <param name="chunk"></param>
        /// <param name="fileId"></param>
        /// <returns></returns>
        public static string GetUrlUploadDowload(int? chunk,string fileId)
        {
            var sbUrlUpload = new StringBuilder();

            var urlBase = Utils.Configuration.GetUrlBaseAnaPlan();

            sbUrlUpload.Append(urlBase);
            sbUrlUpload.Append("/files/");
            sbUrlUpload.Append(fileId);
            sbUrlUpload.Append("/chunks/");

            //  Validate if need append chunk.
            if (chunk != null)
                sbUrlUpload.Append(chunk);

            return sbUrlUpload.ToString();
        }

        /// <summary>
        /// Get the url connect to Anaplan for the get the task for import/export.
        /// </summary>
        /// <param name="typeTask"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public static string GetUrlTaskExec(string typeTask,string id)
        {
            var sbUrlTaskExec = new StringBuilder();

            var urlBase = Utils.Configuration.GetUrlBaseAnaPlan();
            
            sbUrlTaskExec.Append(urlBase);
            sbUrlTaskExec.Append("/");
            sbUrlTaskExec.Append(typeTask);
            sbUrlTaskExec.Append("/");
            //  If the task type is imports set the importID else set exportID.
            sbUrlTaskExec.Append(id);
            sbUrlTaskExec.Append("/tasks/");

            return sbUrlTaskExec.ToString();
        }

        /// <summary>
        /// Get the new instance of HttpClient with the content type application/json into header.
        /// </summary>
        /// <param name="certificateAuthorizationHeader"></param>
        /// <returns></returns>
        public static HttpClient GetHttpClientContentTypeJsonPut(string certificateAuthorizationHeader)
        {
            var httpClient = GetHttpClientAuthorizationHeader(certificateAuthorizationHeader);

            httpClient.DefaultRequestHeaders.TryAddWithoutValidation("accept", "application/json");

            return httpClient;
        }

        /// <summary>
        /// Get the new instance of HttpClient with the content type application/octet-stream into header.
        /// </summary>
        /// <param name="certificateAuthorizationHeader"></param>
        /// <returns></returns>
        public static HttpClient GetHttpClientContentTypeOctetStream(string certificateAuthorizationHeader)
        {
            var httpClient = GetHttpClientAuthorizationHeader(certificateAuthorizationHeader);

            httpClient.DefaultRequestHeaders.TryAddWithoutValidation("accept", "application/octet-stream");

            return httpClient;
        }

        /// <summary>
        /// Get the new instance of HttpClient with the certificate authorization header.
        /// </summary>
        /// <param name="certificateAuthorizationHeader"></param>
        /// <returns></returns>
        public static HttpClient GetHttpClientAuthorizationHeader(string certificateAuthorizationHeader)
        {
            var httpClient = new HttpClient();

            bool useProxy = GetBoolConfiguration("useProxy");

            if (useProxy)
            {
                var handler = new HttpClientHandler
                {
                    UseDefaultCredentials = false,
                    Proxy = new System.Net.WebProxy("proxy.kcc.com", 80),
                    UseProxy = true
                };

                httpClient = new HttpClient(handler);
            }

            httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", certificateAuthorizationHeader);
           

            return httpClient;
        }

        /// <summary>
        /// Get the user ID. If not configured in the web.config get of the server.
        /// </summary>
        /// <param name="User"></param>
        /// <returns></returns>
        public static string GetUserId(System.Security.Principal.IPrincipal User)
        {
            var userId = GetConfiguration("userId");

            if(userId == "")
                userId = User.Identity.Name.Substring(5, 6);

            return userId;
        }

        /// <summary>
        /// Get configuration by key and return a type int.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static int GetIntConfiguration(string key)
        {
            int intResult = 0;

            var resultConfigStr = Utils.Configuration.GetConfiguration(key);

            intResult = int.Parse(resultConfigStr);
            
            return intResult;
        }

        /// <summary>
        /// Get configuration by key and return a type bool.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static bool GetBoolConfiguration(string key)
        {
            bool boolResult = false;

            var resultConfigStr = Utils.Configuration.GetConfiguration(key);

            boolResult = bool.Parse(resultConfigStr);

            return boolResult;
        }

        /// <summary>
        /// Get the configurations into the web.config with the key.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string GetConfiguration(string key)
        {
            var configParameter = ConfigurationManager.AppSettings[key];

            return configParameter;
        }
    }
}