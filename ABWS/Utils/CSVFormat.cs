﻿using System;
using System.Collections.Generic;
using System.Text;
using ABWS.Models;
using System.Data.SqlClient;
using System.Globalization;

namespace ABWS.Utils
{
    public class CSVFormat
    {
        /// <summary>
        /// Get the Cost Center data in format csv from the Cost Center list.
        /// </summary>
        /// <param name="costCenterMDList"></param>
        /// <returns></returns>
        public static string GetCostCenterMD(List<CostCenterMD> costCenterMDList)
        {
            StringBuilder sb = new StringBuilder();

            //  Validate if use header for the data.
            if (Configuration.GetBoolConfiguration("headerGetExcel"))
                sb.Append("cc_code| description| status| LastChangeDate| LastChangeUser\r\n");

            foreach (var itemCCMD in costCenterMDList)
            {
                Append(sb, itemCCMD.CCCode);
                Append(sb, itemCCMD.CCDescription);
                Append(sb, itemCCMD.Status);
                Append(sb, itemCCMD.LastChangeDate.ToString());
                AppendEnd(sb, itemCCMD.LastChangeUser);
            }

            return sb.ToString();
        }

        /// <summary>
        /// Get the Cost Center Versions data in format csv from the version list.
        /// </summary>
        /// <param name="versionList"></param>
        /// <returns></returns>
        public static string GetVersionCostCenter(List<Models.Version> versionList)
        {
            StringBuilder sb = new StringBuilder();

            //  Validate if use header for the data.
            if (Configuration.GetBoolConfiguration("headerGetExcel"))
                sb.Append("Token| Timestamp| User\r\n");

            foreach (var itemVersion in versionList)
            {
                Append(sb, itemVersion.TokenVersion.ToString());
                Append(sb, itemVersion.Timestamp.ToString());
                AppendEnd(sb, itemVersion.UserId);
            }

            return sb.ToString();
        }

        /// <summary>
        /// Get the Financial Data data in format csv from Finacial Data list.
        /// </summary>
        /// <param name="financialDataLst"></param>
        /// <returns></returns>
        public static string GetFinancialData(List<FinancialData> financialDataLst)
        {
            StringBuilder sb = new StringBuilder();

            //  Validate if use header for the data.
            if (Configuration.GetBoolConfiguration("headerGetExcel"))
                sb.Append("Year| Version| CC_CODE| CEH1| CEH2| CEH3| CEH4| CE_CODE| Description| PYJan| PYFeb| PYMar| PYApr| PYMay| PYJun| PYJul| PYAug| PYSep| PYOct| PYNov| PYDec| CYJan| CYFeb| CYMar| CYApr| CYMay| CYJun| CYJul| CYAug| CYSep| CYOct| CYNov| CYDec| NYJan| NYFeb| NYMar| NYApr| NYMay| NYJun| NYJul| NYAug| NYSep| NYOct| NYNov| NYDec\r\n");

            foreach (var financialData in financialDataLst)
            {
                Append(sb, financialData.Year.ToString());
                Append(sb, financialData.Version.ToString());
                Append(sb, financialData.CCCode.ToString());
                Append(sb, financialData.CostElementMD.CEH1.ToString());
                Append(sb, financialData.CostElementMD.CEH2.ToString());
                Append(sb, financialData.CostElementMD.CEH3.ToString());
                Append(sb, financialData.CostElementMD.CEH4.ToString());
                Append(sb, financialData.CostElementMD.CECode.ToString());
                Append(sb, financialData.CostElementMD.Description.ToString());
                Append(sb, financialData.PYJan.ToString());
                Append(sb, financialData.PYFeb.ToString());
                Append(sb, financialData.PYMar.ToString());
                Append(sb, financialData.PYApr.ToString());
                Append(sb, financialData.PYMay.ToString());
                Append(sb, financialData.PYJun.ToString());
                Append(sb, financialData.PYJul.ToString());
                Append(sb, financialData.PYAug.ToString());
                Append(sb, financialData.PYSep.ToString());
                Append(sb, financialData.PYOct.ToString());
                Append(sb, financialData.PYNov.ToString());
                Append(sb, financialData.PYDec.ToString());
                Append(sb, financialData.CYJan.ToString());
                Append(sb, financialData.CYFeb.ToString());
                Append(sb, financialData.CYMar.ToString());
                Append(sb, financialData.CYApr.ToString());
                Append(sb, financialData.CYMay.ToString());
                Append(sb, financialData.CYJun.ToString());
                Append(sb, financialData.CYJul.ToString());
                Append(sb, financialData.CYAug.ToString());
                Append(sb, financialData.CYSep.ToString());
                Append(sb, financialData.CYOct.ToString());
                Append(sb, financialData.CYNov.ToString());
                Append(sb, financialData.CYDec.ToString());
                Append(sb, financialData.NYJan.ToString());
                Append(sb, financialData.NYFeb.ToString());
                Append(sb, financialData.NYMar.ToString());
                Append(sb, financialData.NYApr.ToString());
                Append(sb, financialData.NYMay.ToString());
                Append(sb, financialData.NYJun.ToString());
                Append(sb, financialData.NYJul.ToString());
                Append(sb, financialData.NYAug.ToString());
                Append(sb, financialData.NYSep.ToString());
                Append(sb, financialData.NYOct.ToString());
                Append(sb, financialData.NYNov.ToString());
                AppendEnd(sb, financialData.NYDec.ToString());

            }
            return sb.ToString();
        }

        /// <summary>
        /// Get the Headcount Data data in format csv from Headcount Data list.
        /// </summary>
        /// <param name="headcountlDataLst"></param>
        /// <returns></returns>
        public static string GetHeadCountData(List<HeadCountData> headcountlDataLst)
        {
            StringBuilder sb = new StringBuilder();

            //  Validate if use header for the data.
            if (Configuration.GetBoolConfiguration("headerGetExcel"))
                sb.Append("Year| Version| CC_CODE| HC_CODE| PYJan| PYFeb| PYMar| PYApr| PYMay| PYJun| PYJul| PYAug| PYSep| PYOct| PYNov| PYDec| CYJan| CYFeb| CYMar| CYApr| CYMay| CYJun| CYJul| CYAug| CYSep| CYOct| CYNov| CYDec| NYJan| NYFeb| NYMar| NYApr| NYMay| NYJun| NYJul| NYAug| NYSep| NYOct| NYNov| NYDec \r\n");

            foreach (var headcountlData in headcountlDataLst)
            {
                Append(sb, headcountlData.Year);
                Append(sb, headcountlData.Version);
                Append(sb, headcountlData.CCCode);
                Append(sb, headcountlData.HCCode);
                Append(sb, headcountlData.PYJan.ToString());
                Append(sb, headcountlData.PYFeb.ToString());
                Append(sb, headcountlData.PYMar.ToString());
                Append(sb, headcountlData.PYApr.ToString());
                Append(sb, headcountlData.PYMay.ToString());
                Append(sb, headcountlData.PYJun.ToString());
                Append(sb, headcountlData.PYJul.ToString());
                Append(sb, headcountlData.PYAug.ToString());
                Append(sb, headcountlData.PYSep.ToString());
                Append(sb, headcountlData.PYOct.ToString());
                Append(sb, headcountlData.PYNov.ToString());
                Append(sb, headcountlData.PYDec.ToString());
                Append(sb, headcountlData.CYJan.ToString());
                Append(sb, headcountlData.CYFeb.ToString());
                Append(sb, headcountlData.CYMar.ToString());
                Append(sb, headcountlData.CYApr.ToString());
                Append(sb, headcountlData.CYMay.ToString());
                Append(sb, headcountlData.CYJun.ToString());
                Append(sb, headcountlData.CYJul.ToString());
                Append(sb, headcountlData.CYAug.ToString());
                Append(sb, headcountlData.CYSep.ToString());
                Append(sb, headcountlData.CYOct.ToString());
                Append(sb, headcountlData.CYNov.ToString());
                Append(sb, headcountlData.CYDec.ToString());
                Append(sb, headcountlData.NYJan.ToString());
                Append(sb, headcountlData.NYFeb.ToString());
                Append(sb, headcountlData.NYMar.ToString());
                Append(sb, headcountlData.NYApr.ToString());
                Append(sb, headcountlData.NYMay.ToString());
                Append(sb, headcountlData.NYJun.ToString());
                Append(sb, headcountlData.NYJul.ToString());
                Append(sb, headcountlData.NYAug.ToString());
                Append(sb, headcountlData.NYSep.ToString());
                Append(sb, headcountlData.NYOct.ToString());
                Append(sb, headcountlData.NYNov.ToString());
                AppendEnd(sb, headcountlData.NYDec.ToString());
            }
            return sb.ToString();
        }

        /// <summary>
        /// Get the Cost Element MD data in format csv from Cost Element list.
        /// </summary>
        /// <param name="costElementMDLst"></param>
        /// <returns></returns>
        public static string GetCostElementMD(List<CostElementMD> costElementMDLst)
        {
            StringBuilder sb = new StringBuilder();

            //  Validate if use header for the data.
            if (Configuration.GetBoolConfiguration("headerGetExcel"))
                sb.Append("CE_CODE| Description| CEH1| CEH2| CEH3| CEH4  \r\n");

            foreach (var costElement in costElementMDLst)
            {
                Append(sb, costElement.CECode);
                Append(sb, costElement.Description);
                Append(sb, costElement.CEH1);
                Append(sb, costElement.CEH2);
                Append(sb, costElement.CEH3);
                AppendEnd(sb, costElement.CEH4);
            }
            return sb.ToString();
        }

        /// <summary>
        /// Get the Financial Data data in format csv from Finacial Data list
        /// for send to Anaplan. The result is a list of string by limit bytes.
        /// </summary>
        /// <param name="financialDataLst"></param>
        /// <returns></returns>
        public static List<string> GetFinancialDataForAnaplan(List<FinancialData> financialDataLst)
        {
            StringBuilder sb = new StringBuilder();

            var limit = 500000;
            // Encode the entire string.
            Encoding enc = Encoding.UTF8;
            var auxData = "";
            var financialDataStringLst = new List<string>();
            
            //  Validate if use header for the data.
            if (Configuration.GetBoolConfiguration("headerGetExcel"))
                sb.Append("CC_CODE,CE_CODE,Version,Jun 17,Jul 17,Aug 17,Sep 17,Oct 17,Nov 17,Dec 17,Jan 18,Feb 18,Mar 18,Apr 18,May 18,Jun 18,Jul 18,Aug 18,Sep 18,Oct 18,Nov 18,Dec 18\r\n");

            foreach (var financialData in financialDataLst)
            {
                AppendComma(sb, financialData.CCCode.ToString());
                AppendComma(sb, financialData.CostElementMD.CECode.ToString());
                AppendComma(sb, financialData.Version.ToString());
                AppendComma(sb, financialData.CYJun.ToString());
                AppendComma(sb, financialData.CYJul.ToString());
                AppendComma(sb, financialData.CYAug.ToString());
                AppendComma(sb, financialData.CYSep.ToString());
                AppendComma(sb, financialData.CYOct.ToString());
                AppendComma(sb, financialData.CYNov.ToString());
                AppendComma(sb, financialData.CYDec.ToString());
                AppendComma(sb, financialData.NYJan.ToString());
                AppendComma(sb, financialData.NYFeb.ToString());
                AppendComma(sb, financialData.NYMar.ToString());
                AppendComma(sb, financialData.NYApr.ToString());
                AppendComma(sb, financialData.NYMay.ToString());
                AppendComma(sb, financialData.NYJun.ToString());
                AppendComma(sb, financialData.NYJul.ToString());
                AppendComma(sb, financialData.NYAug.ToString());
                AppendComma(sb, financialData.NYSep.ToString());
                AppendComma(sb, financialData.NYOct.ToString());
                AppendComma(sb, financialData.NYNov.ToString());
                AppendEnd(sb, financialData.NYDec.ToString());
                
                byte[] bytes = enc.GetBytes(sb.ToString());
                //  If the bytes data is greater than limit.
                if(bytes.Length > limit)
                {
                    //  Save the parcial data in the list.
                    financialDataStringLst.Add(auxData);
                    //  clean buffer.
                    sb = new StringBuilder();
                }

                auxData = sb.ToString();
            }

            financialDataStringLst.Add(auxData);

            return financialDataStringLst;
        }

        /// <summary>
        /// Get the Headcount Data in format csv from Headcount Data list
        /// for send to Anaplan. The result is a list of string by limit bytes.
        /// </summary>
        /// <param name="headcountDataLst"></param>
        /// <returns></returns>
        public static List<string> GetHeadcountDataForAnaplan(List<HeadCountData> headcountDataLst)
        {
            StringBuilder sb = new StringBuilder();

            var limit = 500000;
            // Encode the entire string.
            Encoding enc = Encoding.UTF8;
            var auxData = "";
            var headcountDataStringLst = new List<string>();

            //  Validate if use header for the data.
            if (Configuration.GetBoolConfiguration("headerGetExcel"))
                sb.Append("CC_CODE,HC_CODE,Version,Jun 17,Jul 17,Aug 17,Sep 17,Oct 17,Nov 17,Dec 17,Jan 18,Feb 18,Mar 18,Apr 18,May 18,Jun 18,Jul 18,Aug 18,Sep 18,Oct 18,Nov 18,Dec 18\r\n");

            foreach (var headCountData in headcountDataLst)
            {
                AppendComma(sb, headCountData.CCCode.ToString());
                AppendComma(sb, headCountData.HCCode.ToString());
                AppendComma(sb, headCountData.Version.ToString());
                AppendComma(sb, headCountData.CYJun.ToString());
                AppendComma(sb, headCountData.CYJul.ToString());
                AppendComma(sb, headCountData.CYAug.ToString());
                AppendComma(sb, headCountData.CYSep.ToString());
                AppendComma(sb, headCountData.CYOct.ToString());
                AppendComma(sb, headCountData.CYNov.ToString());
                AppendComma(sb, headCountData.CYDec.ToString());
                AppendComma(sb, headCountData.NYJan.ToString());
                AppendComma(sb, headCountData.NYFeb.ToString());
                AppendComma(sb, headCountData.NYMar.ToString());
                AppendComma(sb, headCountData.NYApr.ToString());
                AppendComma(sb, headCountData.NYMay.ToString());
                AppendComma(sb, headCountData.NYJun.ToString());
                AppendComma(sb, headCountData.NYJul.ToString());
                AppendComma(sb, headCountData.NYAug.ToString());
                AppendComma(sb, headCountData.NYSep.ToString());
                AppendComma(sb, headCountData.NYOct.ToString());
                AppendComma(sb, headCountData.NYNov.ToString());
                AppendEnd(sb, headCountData.NYDec.ToString());

                byte[] bytes = enc.GetBytes(sb.ToString());
                //  If the bytes data is greater than limit.
                if (bytes.Length > limit)
                {
                    //  Save the parcial data in the list.
                    headcountDataStringLst.Add(auxData);
                    //  clean buffer.
                    sb = new StringBuilder();
                }

                auxData = sb.ToString();
            }

            headcountDataStringLst.Add(auxData);

            return headcountDataStringLst;
        }

        /// <summary>
        /// Convert the format csv of financial data to a list.
        /// </summary>
        /// <param name="body"></param>
        /// <param name="idUser"></param>
        /// <returns></returns>
        public static List<FinancialData> GetFinancialDataListFromText(string body, string idUser)
        {
            var index = 0;
            var financialDataLst = new List<FinancialData>();

            //  Validate if use header for the data.
            if (Configuration.GetBoolConfiguration("headerPostExcel"))
                index = 1;

            string[] delimiterChars = { "\r\n" };
            string[] lines = body.Split(delimiterChars, StringSplitOptions.RemoveEmptyEntries);

            for (int i = index; i < lines.Length; i++)
            {
                var financialData = GetFinancialDataFromText(lines[i], idUser);

                financialDataLst.Add(financialData);
            }

            return financialDataLst;
        }

        /// <summary>
        /// Convert the format csv of financial data to a list from Anaplan.
        /// </summary>
        /// <param name="import"></param>
        /// <param name="idUser"></param>
        /// <param name="tokenVersion"></param>
        /// <param name="costCenterGlobalLst"></param>
        /// <returns></returns>
        public static List<FinancialDataAnaplan> GetFinancialDataAnaplanListFromText(string import, string idUser, string tokenVersion, List<string> costCenterGlobalLst)
        {
            var currentCCCode = "";

            var db = new DBAccess.DBDataAccess();
            var financialDataLst = new List<FinancialDataAnaplan>();
            var versionLst = new List<Models.Version>();
            //  Get the curret date.
            var timestamp = DateTime.Now;

            //  Get the array of Financial Data.
            string[] lines = GetArrayStringFromText(import,1);
            //  Iterate the array of Financial Data.
            for (int i = 0; i < lines.Length - 1; i++)
            {
                var financialData = GetFinancialDataAnaplanFromText(lines[i], idUser,timestamp,tokenVersion);
                //  If the Cost Center code is distinct.
                if (!currentCCCode.Equals(financialData.CC_CODE) && !costCenterGlobalLst.Contains(financialData.CC_CODE))
                {
                    currentCCCode = financialData.CC_CODE;
                    //  Create the object version with the token cost center code, user and timestamp.
                    var version = new Models.Version(currentCCCode, tokenVersion, idUser, timestamp);
                    version.AnaplanTimestamp = timestamp;
                    // Add to list the new token version.
                    versionLst.Add(version);
                    costCenterGlobalLst.Add(currentCCCode);
                }
                
                //  Add the financial data to list.
                financialDataLst.Add(financialData);
            }
            //  insert the version list by cost center.
            db.InsertData(versionLst, "Version");

            return financialDataLst;
        }

        /// <summary>
        /// Convert the format csv of Financial Data to a list from Anaplan.
        /// </summary>
        /// <param name="import"></param>
        /// <param name="idUser"></param>
        /// <param name="tokenVersion"></param>
        /// <returns></returns>
        public static List<HeadcountDataAnaplan> GetHeadcountDataAnaplanListFromText(string import, string idUser, string tokenVersion)
        {
            var headcountDataLst = new List<HeadcountDataAnaplan>();

            //  Get the array of Financial Data.
            string[] lines = GetArrayStringFromText(import,1);
            var timestamp = DateTime.Now;

            for (int i = 0; i < lines.Length - 1; i++)
            {
                var headcountData = GetHeadcountDataAnaplanFromText(lines[i], idUser, timestamp, tokenVersion);
                
                //  Add the Financial Data to list.
                headcountDataLst.Add(headcountData);
            }

            return headcountDataLst;
        }
        
        /// <summary>
        /// Get a object financial data from a string line.
        /// </summary>
        /// <param name="line"></param>
        /// <param name="idUser"></param>
        /// <returns></returns>
        public static FinancialData GetFinancialDataFromText(string line, string idUser)
        {
            char[] delimiterChars = { ';' };
            string[] values;

            var financialData = new FinancialData();
            var costElement = new CostElementMD();

            values = line.Split(delimiterChars);

            financialData.Version = values[1];
            financialData.CCCode = values[2];

            costElement.CECode = values[3];
            financialData.CostElementMD = costElement;

            financialData.Year = values[0];

            financialData.PYJan = GetDouble(values[4]);
            financialData.PYFeb = GetDouble(values[5]);
            financialData.PYMar = GetDouble(values[6]);
            financialData.PYApr = GetDouble(values[7]);
            financialData.PYMay = GetDouble(values[8]);
            financialData.PYJun = GetDouble(values[9]);
            financialData.PYJul = GetDouble(values[10]);
            financialData.PYAug = GetDouble(values[11]);
            financialData.PYSep = GetDouble(values[12]);
            financialData.PYOct = GetDouble(values[13]);
            financialData.PYNov = GetDouble(values[14]);
            financialData.PYDec = GetDouble(values[15]);

            financialData.CYJan = GetDouble(values[16]);
            financialData.CYFeb = GetDouble(values[17]);
            financialData.CYMar = GetDouble(values[18]);
            financialData.CYApr = GetDouble(values[19]);
            financialData.CYMay = GetDouble(values[20]);
            financialData.CYJun = GetDouble(values[21]);
            financialData.CYJul = GetDouble(values[22]);
            financialData.CYAug = GetDouble(values[23]);
            financialData.CYSep = GetDouble(values[24]);
            financialData.CYOct = GetDouble(values[25]);
            financialData.CYNov = GetDouble(values[26]);
            financialData.CYDec = GetDouble(values[27]);

            financialData.NYJan = GetDouble(values[28]);
            financialData.NYFeb = GetDouble(values[29]);
            financialData.NYMar = GetDouble(values[30]);
            financialData.NYApr = GetDouble(values[31]);
            financialData.NYMay = GetDouble(values[32]);
            financialData.NYJun = GetDouble(values[33]);
            financialData.NYJul = GetDouble(values[34]);
            financialData.NYAug = GetDouble(values[35]);
            financialData.NYSep = GetDouble(values[36]);
            financialData.NYOct = GetDouble(values[37]);
            financialData.NYNov = GetDouble(values[38]);
            financialData.NYDec = GetDouble(values[39]);

            financialData.ChangeDate = DateTime.Now;
            financialData.ChangeUser = idUser;

            return financialData;
        }

        /// <summary>
        /// Get a object financial data from a string line.
        /// The data from Anaplan.
        /// </summary>
        /// <param name="line"></param>
        /// <param name="idUser"></param>
        /// <param name="timestamp"></param>
        /// <param name="tokenVersion"></param>
        /// <returns></returns>
        public static FinancialDataAnaplan GetFinancialDataAnaplanFromText(string line, string idUser,DateTime timestamp,string tokenVersion)
        {
            char[] delimiterChars = { ',' };
            string[] values;
            
            var financialData = new FinancialDataAnaplan();
            
            values = line.Split(delimiterChars);

            financialData.CC_CODE = values[0].Substring(9, 5);

            financialData.CE_CODE = values[1];
            
            financialData.Version = values[2];
            
            financialData.Year = values[3];

            financialData.PYJan = GetDouble(values[5]);
            financialData.PYFeb = GetDouble(values[6]);
            financialData.PYMar = GetDouble(values[7]);
            financialData.PYApr = GetDouble(values[8]);
            financialData.PYMay = GetDouble(values[9]);
            financialData.PYJun = GetDouble(values[10]);
            financialData.PYJul = GetDouble(values[11]);
            financialData.PYAug = GetDouble(values[12]);
            financialData.PYSep = GetDouble(values[13]);
            financialData.PYOct = GetDouble(values[14]);
            financialData.PYNov = GetDouble(values[15]);
            financialData.PYDec = GetDouble(values[16]);

            financialData.CYJan = GetDouble(values[17]);
            financialData.CYFeb = GetDouble(values[18]);
            financialData.CYMar = GetDouble(values[19]);
            financialData.CYApr = GetDouble(values[20]);
            financialData.CYMay = GetDouble(values[21]);
            financialData.CYJun = GetDouble(values[22]);
            financialData.CYJul = GetDouble(values[23]);
            financialData.CYAug = GetDouble(values[24]);
            financialData.CYSep = GetDouble(values[25]);
            financialData.CYOct = GetDouble(values[26]);
            financialData.CYNov = GetDouble(values[27]);
            financialData.CYDec = GetDouble(values[28]);

            financialData.NYJan = GetDouble(values[29]);
            financialData.NYFeb = GetDouble(values[30]);
            financialData.NYMar = GetDouble(values[31]);
            financialData.NYApr = GetDouble(values[32]);
            financialData.NYMay = GetDouble(values[33]);
            financialData.NYJun = GetDouble(values[34]);
            financialData.NYJul = GetDouble(values[35]);
            financialData.NYAug = GetDouble(values[36]);
            financialData.NYSep = GetDouble(values[37]);
            financialData.NYOct = GetDouble(values[38]);
            financialData.NYNov = GetDouble(values[39]);
            financialData.NYDec = GetDouble(values[40]);

            financialData.ChangeDate = timestamp;
            financialData.ChangeUser = idUser;
            financialData.TokenVersion = tokenVersion;

            return financialData;
        }

        /// <summary>
        /// Get a object Financial Data from a string line.
        /// The data from Anaplan.
        /// </summary>
        /// <param name="line"></param>
        /// <param name="idUser"></param>
        /// <param name="timestamp"></param>
        /// <param name="tokenVersion"></param>
        /// <returns></returns>
        public static HeadcountDataAnaplan GetHeadcountDataAnaplanFromText(string line, string idUser,DateTime timestamp ,string tokenVersion)
        {
            char[] delimiterChars = { ',' };
            string[] values;

            var headcountData = new HeadcountDataAnaplan();
            
            values = line.Split(delimiterChars);

            headcountData.CC_CODE = values[0].Substring(9, 5);

            headcountData.HC_CODE = values[1];

            headcountData.Version = values[2];

            headcountData.Year = values[3];

            headcountData.PYJan = GetDouble(values[3]);
            headcountData.PYFeb = GetDouble(values[4]);
            headcountData.PYMar = GetDouble(values[5]);
            headcountData.PYApr = GetDouble(values[6]);
            headcountData.PYMay = GetDouble(values[7]);
            headcountData.PYJun = GetDouble(values[8]);
            headcountData.PYJul = GetDouble(values[9]);
            headcountData.PYAug = GetDouble(values[10]);
            headcountData.PYSep = GetDouble(values[11]);
            headcountData.PYOct = GetDouble(values[12]);
            headcountData.PYNov = GetDouble(values[13]);
            headcountData.PYDec = GetDouble(values[14]);

            headcountData.CYJan = GetDouble(values[15]);
            headcountData.CYFeb = GetDouble(values[16]);
            headcountData.CYMar = GetDouble(values[17]);
            headcountData.CYApr = GetDouble(values[18]);
            headcountData.CYMay = GetDouble(values[19]);
            headcountData.CYJun = GetDouble(values[20]);
            headcountData.CYJul = GetDouble(values[21]);
            headcountData.CYAug = GetDouble(values[22]);
            headcountData.CYSep = GetDouble(values[23]);
            headcountData.CYOct = GetDouble(values[24]);
            headcountData.CYNov = GetDouble(values[25]);
            headcountData.CYDec = GetDouble(values[26]);

            headcountData.NYJan = GetDouble(values[27]);
            headcountData.NYFeb = GetDouble(values[28]);
            headcountData.NYMar = GetDouble(values[29]);
            headcountData.NYApr = GetDouble(values[30]);
            headcountData.NYMay = GetDouble(values[31]);
            headcountData.NYJun = GetDouble(values[32]);
            headcountData.NYJul = GetDouble(values[33]);
            headcountData.NYAug = GetDouble(values[34]);
            headcountData.NYSep = GetDouble(values[35]);
            headcountData.NYOct = GetDouble(values[36]);
            headcountData.NYNov = GetDouble(values[37]);
            headcountData.NYDec = GetDouble(values[38]);

            headcountData.ChangeDate = timestamp;
            headcountData.ChangeUser = idUser;
            headcountData.TokenVersion = tokenVersion;

            return headcountData;
        }

        /// <summary>
        /// Convert the format csv of head count to a list.
        /// </summary>
        /// <param name="body"></param>
        /// <param name="idUser"></param>
        /// <returns></returns>
        public static List<HeadCountData> GetHeadCountDataListFromText(string body, string idUser)
        {
            var index = 0;
            var headCounDataLst = new List<HeadCountData>();

            //  Validate if use header for the data.
            if (Configuration.GetBoolConfiguration("headerPostExcel"))
                index = 1;

            string[] delimiterChars = { "\r\n" };
            string[] lines = body.Split(delimiterChars, StringSplitOptions.RemoveEmptyEntries);

            for (int i = index; i < lines.Length; i++)
            {
                var headcountData = GetHeadCountDataFromText(lines[i], idUser);

                headCounDataLst.Add(headcountData);
            }

            return headCounDataLst;
        }

        /// <summary>
        /// Get a object Headcount Data from a string line.
        /// </summary>
        /// <param name="line"></param>
        /// <param name="idUser"></param>
        /// <returns></returns>
        public static HeadCountData GetHeadCountDataFromText(string line, string idUser)
        {
            char[] delimiterChars = { ';' };
            string[] values;

            var headcountData = new HeadCountData();

            values = line.Split(delimiterChars);

            headcountData.Year = values[0];
            headcountData.Version = values[1];
            headcountData.CCCode = values[2];
            headcountData.HCCode = values[3];

            headcountData.PYJan = GetDouble(values[4]);
            headcountData.PYFeb = GetDouble(values[5]);
            headcountData.PYMar = GetDouble(values[6]);
            headcountData.PYApr = GetDouble(values[7]);
            headcountData.PYMay = GetDouble(values[8]);
            headcountData.PYJun = GetDouble(values[9]);
            headcountData.PYJul = GetDouble(values[10]);
            headcountData.PYAug = GetDouble(values[11]);
            headcountData.PYSep = GetDouble(values[12]);
            headcountData.PYOct = GetDouble(values[13]);
            headcountData.PYNov = GetDouble(values[14]);
            headcountData.PYDec = GetDouble(values[15]);

            headcountData.CYJan = GetDouble(values[16]);
            headcountData.CYFeb = GetDouble(values[17]);
            headcountData.CYMar = GetDouble(values[18]);
            headcountData.CYApr = GetDouble(values[19]);
            headcountData.CYMay = GetDouble(values[20]);
            headcountData.CYJun = GetDouble(values[21]);
            headcountData.CYJul = GetDouble(values[22]);
            headcountData.CYAug = GetDouble(values[23]);
            headcountData.CYSep = GetDouble(values[24]);
            headcountData.CYOct = GetDouble(values[25]);
            headcountData.CYNov = GetDouble(values[26]);
            headcountData.CYDec = GetDouble(values[27]);

            headcountData.NYJan = GetDouble(values[28]);
            headcountData.NYFeb = GetDouble(values[29]);
            headcountData.NYMar = GetDouble(values[30]);
            headcountData.NYApr = GetDouble(values[31]);
            headcountData.NYMay = GetDouble(values[32]);
            headcountData.NYJun = GetDouble(values[33]);
            headcountData.NYJul = GetDouble(values[34]);
            headcountData.NYAug = GetDouble(values[35]);
            headcountData.NYSep = GetDouble(values[36]);
            headcountData.NYOct = GetDouble(values[37]);
            headcountData.NYNov = GetDouble(values[38]);
            headcountData.NYDec = GetDouble(values[39]);

            headcountData.ChangeDate = DateTime.Now;
            headcountData.ChangeUser = idUser;

            return headcountData;
        }

        /// <summary>
        /// Convert the format csv of head count to a list.
        /// </summary>
        /// <param name="body"></param>
        /// <param name="cc_code"></param>
        /// <param name="tokenVersion"></param>
        /// <param name="financialDataOld"></param>
        /// <returns></returns>
        public static List<FinancialData> GetFinancialDataWithNewCostElementListFromText(string body, string cc_code, string tokenVersion, FinancialData financialDataOld)
        {
            var index = 0;
            var financialDataLst = new List<FinancialData>();

            //  Validate if use header for the data.
            if (Configuration.GetBoolConfiguration("headerPostExcel"))
                index = 1;

            string[] delimiterChars = { "\r\n" };

            string[] lines = body.Split(delimiterChars, StringSplitOptions.RemoveEmptyEntries);

            for (int i = index; i < lines.Length; i++)
            {
                var costElement = GetCostElementFromText(lines[i]);
                var financialData = new FinancialData();

                financialData.Version = financialDataOld.Version;
                financialData.CCCode = cc_code;

                costElement.CECode = costElement.CECode;
                financialData.CostElementMD = costElement;

                financialData.Year = financialDataOld.Year;

                financialData.PYJan = 0;
                financialData.PYFeb = 0;
                financialData.PYMar = 0;
                financialData.PYApr = 0;
                financialData.PYMay = 0;
                financialData.PYJun = 0;
                financialData.PYJul = 0;
                financialData.PYAug = 0;
                financialData.PYSep = 0;
                financialData.PYOct = 0;
                financialData.PYNov = 0;
                financialData.PYDec = 0;

                financialData.CYJan = 0;
                financialData.CYFeb = 0;
                financialData.CYMar = 0;
                financialData.CYApr = 0;
                financialData.CYMay = 0;
                financialData.CYJun = 0;
                financialData.CYJul = 0;
                financialData.CYAug = 0;
                financialData.CYSep = 0;
                financialData.CYOct = 0;
                financialData.CYNov = 0;
                financialData.CYDec = 0;

                financialData.NYJan = 0;
                financialData.NYFeb = 0;
                financialData.NYMar = 0;
                financialData.NYApr = 0;
                financialData.NYMay = 0;
                financialData.NYJun = 0;
                financialData.NYJul = 0;
                financialData.NYAug = 0;
                financialData.NYSep = 0;
                financialData.NYOct = 0;
                financialData.NYNov = 0;
                financialData.NYDec = 0;

                financialData.ChangeDate = financialDataOld.ChangeDate;
                financialData.ChangeUser = financialDataOld.ChangeUser;
                financialData.TokenVersion = tokenVersion;

                financialDataLst.Add(financialData);
            }

            return financialDataLst;
        }

        /// <summary>
        /// Get a object cost element from a string line.
        /// </summary>
        /// <param name="line"></param>
        /// <returns></returns>
        public static CostElementMD GetCostElementFromText(string line)
        {
            char[] delimiterChars = { ',' };
            string[] values;

            var costElement = new CostElementMD();

            values = line.Split(delimiterChars);

            costElement.CECode = values[0];

            return costElement;
        }

        /// <summary>
        /// Get column name of Financial or Headcount Data from SqlDataReader.
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
        public static string GetHeaderCsvFinancialHeadcountDataForAnaPlan(SqlDataReader reader)
        {
            var count = reader.FieldCount;
            StringBuilder sb = new StringBuilder();
            var header = "";
            
            for (var j = 0; j < count; j++)
            {
                AppendComma(sb,reader.GetName(j));
            }

            header = sb.ToString().Substring(0, sb.ToString().Length - 1) + "\r\n";

            return header;
        }

        /// <summary>
        /// Get data of Financial or Headcount Data from SqlDataReader.
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
        public static string GetDataCsvFinancialHeadcountDataForAnaPlan(SqlDataReader reader)
        {
            var count = reader.FieldCount;
            StringBuilder sb = new StringBuilder();
            var data = "";

            var ccCode = reader.GetString(0);
            var ce_he_Code = reader.GetString(1);
            var version = reader.GetString(2);

            AppendComma(sb, ccCode);
            AppendComma(sb, ce_he_Code);
            AppendComma(sb, version);
            
            for (var i = 3; i < count; i++)
            {
                var value = reader.GetDouble(i).ToString();
                AppendComma(sb,value);
            }

            data = sb.ToString().Substring(0, sb.ToString().Length - 1) + "\r\n";
            
            return data;
        }
        


        /// <summary>
        /// Convert the format csv of financial data to array from Anaplan.
        /// </summary>
        /// <param name="import"></param>
        /// <param name="beginIndex"></param>
        /// <returns></returns>
        public static string[] GetArrayStringFromText(string import, int beginIndex)
        {
            char[] delimiterChars = { '\n' };

            //  Get the array of Financial Data.
            string[] input = import.Split(delimiterChars);
            //  New array with Financial Data to insert data base.
            string[] lines = new string[input.Length - beginIndex];
            //  Iterate the original data of Financial Data.
            for (int i = beginIndex; i < input.Length - 1; i++)
            {
                //  Get each row.
                input[i] = input[i].Substring(0, input[i].Length - 1);
                //  Set to new array Financial Data the row selected.
                lines[i - beginIndex] = input[i];
            }

            return lines;
        }

        /// <summary>
        /// Append a string value to StringBuilder parameter.
        /// </summary>
        /// <param name="sb"></param>
        /// <param name="value"></param>
        /// <param name="delimiter"></param>
        public static void Append(StringBuilder sb, string value, string delimiter = "|")
        {
            if (value == null)
                value = "";

            sb.Append(value + delimiter);
        }

        /// <summary>
        /// Append a string value to StringBuilder parameter with comma.
        /// </summary>
        /// <param name="sb"></param>
        /// <param name="value"></param>
        public static void AppendComma(StringBuilder sb, string value)
        {
            Append(sb, value, ",");
        }

        /// <summary>
        /// Append a string value to StringBuilder parameter and add line jump.
        /// </summary>
        /// <param name="sb"></param>
        /// <param name="value"></param>
        public static void AppendEnd(StringBuilder sb, string value)
        {
            sb.Append(value);
            sb.Append("\r\n");
        }
        
        /// <summary>
        /// Convert a string value to double value.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static double GetDouble(string value)
        {
            double result = 0;
            float resultFloat = 0;
            double resultDouble = 0;
            
            bool isNumeric = float.TryParse(value, out resultFloat);

            if (!isNumeric)
            {
                result = 0;
            }
            else
            {
                //  Parse to Double.
                value = value.Replace(',', '.');
                double.TryParse(value, NumberStyles.Any, CultureInfo.InvariantCulture, out resultDouble);
                result = resultDouble;
            }

            return result;
        }

    }
}