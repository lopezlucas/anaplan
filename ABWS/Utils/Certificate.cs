﻿using System;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace ABWS.Utils
{
    public class Certificate
    {
        public static string GetCertificateAuthorizationHeader()
        {
            //string path = System.Web.Hosting.HostingEnvironment.MapPath(@"~/App_Data/certificate-140799994400172148355298908232795014204.cer");
            string path = System.Web.Configuration.WebConfigurationManager.AppSettings["certpath"].ToString();
            //string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, relativePath);          
            //string path = System.Web.Configuration.WebConfigurationManager.AppSettings["certpath"].ToString();
            X509Certificate cert = new X509Certificate(path);

            string certificateCommonName = cert.Subject.Substring(3, cert.Subject.Length - 3);

            byte[] certEncoded = cert.Export(X509ContentType.Cert);


            string base64 = Convert.ToBase64String(cert.Export(X509ContentType.Cert), Base64FormattingOptions.None);
            base64 = Convert.ToBase64String(cert.GetRawCertData());

            StringBuilder builder = new StringBuilder();
            builder.Append("-----BEGIN CERTIFICATE-----\n");
            builder.Append(base64);
            builder.Append("\n-----END CERTIFICATE-----");

            string rawAuthenticationParameter = certificateCommonName + ":" + builder.ToString();
            byte[] rawAuthByte = Encoding.UTF8.GetBytes(rawAuthenticationParameter);
            Encoding enc = new UTF8Encoding(true, true);
            byte[] rawAuthByte1 = enc.GetBytes(rawAuthenticationParameter);

            return "AnaplanCertificate " + Convert.ToBase64String(rawAuthByte, Base64FormattingOptions.None);
        }
    }
}