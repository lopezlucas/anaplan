﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using ABWS.Models;
using System.ComponentModel;

namespace ABWS.DBAccess
{
    public class DBDataAccess
    {
        private string strConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;

        #region Cost Center

        /// <summary>
        /// Get the Cost Center data by User ID.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public List<CostCenterMD> GetCCByUserId(string userId)
        {
            var costCenterMDList = new List<CostCenterMD>();
            SqlConnection objSQLConnection = new SqlConnection(strConnectionString);

            try
            {
                SqlDataReader reader;
                SqlCommand objSQLCmd = new SqlCommand();

                objSQLCmd.CommandText = "spGetCostCenterMDByUserId";
                objSQLCmd.CommandType = CommandType.StoredProcedure;
                objSQLCmd.Parameters.Add("@UserId", SqlDbType.NVarChar, 8).Value = userId;

                objSQLCmd.Connection = objSQLConnection;

                objSQLConnection.Open();
                reader = objSQLCmd.ExecuteReader();

                while (reader.Read())
                {
                    var costCenterMD = new CostCenterMD(reader);

                    costCenterMDList.Add(costCenterMD);
                }

                return costCenterMDList;
            }
            catch (Exception ex)
            {
                throw new Exception("Error in SQL:" + ex);
            }
            finally
            {
                objSQLConnection.Close();
            }
        }

        /// <summary>
        /// Check if exist Cost Center.
        /// </summary>
        /// <param name="cc_code"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public bool CheckUserByCC(string cc_code, string userId)
        {
            SqlConnection objSQLConnection = new SqlConnection(strConnectionString);
            var exist = false;

            try
            {
                SqlDataReader reader;
                SqlCommand objSQLCmd = new SqlCommand();

                objSQLCmd.CommandText = "spExistCostCenter";
                objSQLCmd.CommandType = CommandType.StoredProcedure;

                objSQLCmd.Parameters.Add("@CC_CODE", SqlDbType.NVarChar, 50).Value = cc_code;
                objSQLCmd.Parameters.Add("@UserId", SqlDbType.NVarChar, 8).Value = userId;

                objSQLCmd.Connection = objSQLConnection;

                objSQLConnection.Open();
                reader = objSQLCmd.ExecuteReader();

                while (reader.Read())
                {
                    //  If the value is greater than zero the user and code exist.
                    exist = reader.GetInt32(0) > 0;
                }

                return exist;
            }
            catch (Exception ex)
            {
                throw new Exception("Error in SQL:" + ex);
            }
            finally
            {
                objSQLConnection.Close();
            }
        }

        /// <summary>
        /// Get the Cost Center data by Cost Center Code.
        /// </summary>
        /// <param name="cc_code"></param>
        /// <returns></returns>
        public List<CostCenterMD> GetCCByCCCode(string cc_code)
        {
            var costCenterMDList = new List<CostCenterMD>();
            SqlConnection objSQLConnection = new SqlConnection(strConnectionString);

            try
            {
                SqlDataReader reader;
                SqlCommand objSQLCmd = new SqlCommand();

                objSQLCmd.CommandText = "spGetCostCenterMDByCCCode";
                objSQLCmd.CommandType = CommandType.StoredProcedure;

                objSQLCmd.Parameters.Add("@CC_CODE", SqlDbType.NVarChar, 50).Value = cc_code;

                objSQLCmd.Connection = objSQLConnection;

                objSQLConnection.Open();
                reader = objSQLCmd.ExecuteReader();

                while (reader.Read())
                {
                    var costCenterMD = new CostCenterMD(reader);

                    costCenterMDList.Add(costCenterMD);
                }

                return costCenterMDList;
            }
            catch (Exception ex)
            {
                throw new Exception("Error in SQL:" + ex);
            }
            finally
            {
                objSQLConnection.Close();
            }
        }

        /// <summary>
        /// Get Version Cost Center by Cost Center Code.
        /// </summary>
        /// <param name="cc_code"></param>
        /// <returns></returns>
        public List<Models.Version> GetVersionCCByCCCode(string cc_code)
        {
            var versionList = new List<Models.Version>();
            SqlConnection objSQLConnection = new SqlConnection(strConnectionString);

            try
            {
                SqlDataReader reader;
                SqlCommand objSQLCmd = new SqlCommand();

                objSQLCmd.CommandText = "spGetCostCenterMDVersionByCCCode";
                objSQLCmd.CommandType = CommandType.StoredProcedure;

                objSQLCmd.Parameters.Add("@CC_CODE", SqlDbType.NVarChar, 50).Value = cc_code;

                objSQLCmd.Connection = objSQLConnection;

                objSQLConnection.Open();
                reader = objSQLCmd.ExecuteReader();

                while (reader.Read())
                {
                    var version = new Models.Version();

                    version.TokenVersion = reader.GetString(0);
                    version.Timestamp = reader.GetDateTime(1);
                    version.UserId = reader.GetString(2);

                    versionList.Add(version);
                }

                return versionList;
            }
            catch (Exception ex)
            {
                throw new Exception("Error in SQL:" + ex);
            }
            finally
            {
                objSQLConnection.Close();
            }
        }

        /// <summary>
        /// Update last change of cost center.
        /// </summary>
        /// <param name="cc_code"></param>
        /// <param name="lastChangeUser"></param>
        /// <param name="lastChangeDate"></param>
        public void UpdateLastChangeCostCenter(string cc_code,string lastChangeUser, DateTime lastChangeDate)
        {
            SqlConnection objSQLConnection = new SqlConnection(strConnectionString);
            
            try
            {
                SqlCommand objSQLCmd = new SqlCommand();

                objSQLCmd.CommandText = "spUpdateLastChangeCostCenter";
                objSQLCmd.CommandType = CommandType.StoredProcedure;
                
                objSQLCmd.Parameters.Add("@CC_Code", SqlDbType.NVarChar, 50).Value = cc_code;
                objSQLCmd.Parameters.Add("@LastChangeUser", SqlDbType.NVarChar, 8).Value = lastChangeUser;
                objSQLCmd.Parameters.Add("@LastChangeDate", SqlDbType.DateTime).Value = lastChangeDate;

                objSQLCmd.Connection = objSQLConnection;

                objSQLConnection.Open();
                objSQLCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw new Exception("Error in SQL:" + ex);
            }
            finally
            {
                objSQLConnection.Close();
            }
        }

        #endregion

        #region Financial Data

        /// <summary>
        /// Get version financial entries associated to a cost center and version.
        /// </summary>
        /// <param name="cc_code"></param>
        /// <param name="tokenVersion"></param>
        /// <param name="userId"></param> //optional
        /// <returns></returns>
        public List<FinancialData> GetVersionFDByCCCode(string cc_code, string tokenVersion, string userId = null)
        {
            var financialDataLst = new List<FinancialData>();
            SqlConnection objSQLConnection = new SqlConnection(strConnectionString);
            var serverDate = DateTime.Now;
            try
            {
                SqlDataReader reader;
                SqlCommand objSQLCmd = new SqlCommand();

                objSQLCmd.CommandText = "spGetFinancialDataCostElementVersionByCCCode";

                objSQLCmd.CommandType = CommandType.StoredProcedure;

                objSQLCmd.Parameters.Add("@CC_CODE", SqlDbType.NVarChar, 50).Value = cc_code;
                objSQLCmd.Parameters.Add("@TokenVersion", SqlDbType.NVarChar, 50).Value = tokenVersion;

                objSQLCmd.Connection = objSQLConnection;

                objSQLConnection.Open();
                reader = objSQLCmd.ExecuteReader();

                while (reader.Read())
                {
                    var financialData = new FinancialData(reader);

                    financialData.ChangeDate = serverDate;
                    financialData.ChangeUser = userId;

                    financialDataLst.Add(financialData);
                }

                return financialDataLst;
            }
            catch (Exception ex)
            {
                throw new Exception("Error in SQL:" + ex);
            }
            finally
            {
                objSQLConnection.Close();
            }
        }

        /// <summary>
        /// Insert financial entries.
        /// </summary>
        /// <param name="financialDataLst"></param>
        /// <param name="token"></param>
        public void InsertFinancialData(List<FinancialData> financialDataLst, string token)
        {
            SqlConnection objSQLConnection = new SqlConnection(strConnectionString);
            SqlTransaction objSQLTrx;

            SqlCommand objSQLCmd = objSQLConnection.CreateCommand();
            objSQLConnection.Open();
            objSQLTrx = objSQLConnection.BeginTransaction();

            try
            {
                objSQLCmd.CommandType = CommandType.StoredProcedure;
                objSQLCmd.Transaction = objSQLTrx;

                foreach (var itemFinancialData in financialDataLst)
                {
                    objSQLCmd.CommandText = "spInsertFinancialData";

                    objSQLCmd.Parameters.Clear();
                    objSQLCmd.Parameters.Add("@CC_Code", SqlDbType.NVarChar, 50).Value = itemFinancialData.CCCode;
                    objSQLCmd.Parameters.Add("@CE_Code", SqlDbType.NVarChar, 50).Value = itemFinancialData.CostElementMD.CECode;
                    objSQLCmd.Parameters.Add("@Version", SqlDbType.NVarChar, 50).Value = itemFinancialData.Version;
                    objSQLCmd.Parameters.Add("@Year", SqlDbType.NVarChar, 4).Value = itemFinancialData.Year;
                    objSQLCmd.Parameters.Add("@PYJan", SqlDbType.Float).Value = itemFinancialData.PYJan;
                    objSQLCmd.Parameters.Add("@PYFeb", SqlDbType.Float).Value = itemFinancialData.PYFeb;
                    objSQLCmd.Parameters.Add("@PYMar", SqlDbType.Float).Value = itemFinancialData.PYMar;
                    objSQLCmd.Parameters.Add("@PYApr", SqlDbType.Float).Value = itemFinancialData.PYApr;
                    objSQLCmd.Parameters.Add("@PYMay", SqlDbType.Float).Value = itemFinancialData.PYMay;
                    objSQLCmd.Parameters.Add("@PYJun", SqlDbType.Float).Value = itemFinancialData.PYJun;
                    objSQLCmd.Parameters.Add("@PYJul", SqlDbType.Float).Value = itemFinancialData.PYJul;
                    objSQLCmd.Parameters.Add("@PYAug", SqlDbType.Float).Value = itemFinancialData.PYAug;
                    objSQLCmd.Parameters.Add("@PYSep", SqlDbType.Float).Value = itemFinancialData.PYSep;
                    objSQLCmd.Parameters.Add("@PYOct", SqlDbType.Float).Value = itemFinancialData.PYOct;
                    objSQLCmd.Parameters.Add("@PYNov", SqlDbType.Float).Value = itemFinancialData.NYNov;
                    objSQLCmd.Parameters.Add("@PYDec", SqlDbType.Float).Value = itemFinancialData.PYDec;
                    objSQLCmd.Parameters.Add("@CYJan", SqlDbType.Float).Value = itemFinancialData.CYJan;
                    objSQLCmd.Parameters.Add("@CYFeb", SqlDbType.Float).Value = itemFinancialData.CYFeb;
                    objSQLCmd.Parameters.Add("@CYMar", SqlDbType.Float).Value = itemFinancialData.CYMar;
                    objSQLCmd.Parameters.Add("@CYApr", SqlDbType.Float).Value = itemFinancialData.CYApr;
                    objSQLCmd.Parameters.Add("@CYMay", SqlDbType.Float).Value = itemFinancialData.CYMay;
                    objSQLCmd.Parameters.Add("@CYJun", SqlDbType.Float).Value = itemFinancialData.CYJun;
                    objSQLCmd.Parameters.Add("@CYJul", SqlDbType.Float).Value = itemFinancialData.CYJul;
                    objSQLCmd.Parameters.Add("@CYAug", SqlDbType.Float).Value = itemFinancialData.CYAug;
                    objSQLCmd.Parameters.Add("@CYSep", SqlDbType.Float).Value = itemFinancialData.CYSep;
                    objSQLCmd.Parameters.Add("@CYOct", SqlDbType.Float).Value = itemFinancialData.CYOct;
                    objSQLCmd.Parameters.Add("@CYNov", SqlDbType.Float).Value = itemFinancialData.CYNov;
                    objSQLCmd.Parameters.Add("@CYDec", SqlDbType.Float).Value = itemFinancialData.CYDec;
                    objSQLCmd.Parameters.Add("@NYJan", SqlDbType.Float).Value = itemFinancialData.NYJan;
                    objSQLCmd.Parameters.Add("@NYFeb", SqlDbType.Float).Value = itemFinancialData.NYFeb;
                    objSQLCmd.Parameters.Add("@NYMar", SqlDbType.Float).Value = itemFinancialData.NYMar;
                    objSQLCmd.Parameters.Add("@NYApr", SqlDbType.Float).Value = itemFinancialData.NYApr;
                    objSQLCmd.Parameters.Add("@NYMay", SqlDbType.Float).Value = itemFinancialData.NYMay;
                    objSQLCmd.Parameters.Add("@NYJun", SqlDbType.Float).Value = itemFinancialData.NYJun;
                    objSQLCmd.Parameters.Add("@NYJul", SqlDbType.Float).Value = itemFinancialData.NYJul;
                    objSQLCmd.Parameters.Add("@NYAug", SqlDbType.Float).Value = itemFinancialData.NYAug;
                    objSQLCmd.Parameters.Add("@NYSep", SqlDbType.Float).Value = itemFinancialData.NYSep;
                    objSQLCmd.Parameters.Add("@NYOct", SqlDbType.Float).Value = itemFinancialData.NYOct;
                    objSQLCmd.Parameters.Add("@NYNov", SqlDbType.Float).Value = itemFinancialData.NYNov;
                    objSQLCmd.Parameters.Add("@NYDec", SqlDbType.Float).Value = itemFinancialData.NYDec;
                    objSQLCmd.Parameters.Add("@ChangeDate", SqlDbType.DateTime).Value = itemFinancialData.ChangeDate;
                    objSQLCmd.Parameters.Add("@ChangeUser", SqlDbType.NVarChar, 8).Value = itemFinancialData.ChangeUser;
                    objSQLCmd.Parameters.Add("@TokenVersion", SqlDbType.NVarChar, 50).Value = token;

                    objSQLCmd.ExecuteNonQuery();
                }

                UpdateVersionFD(token, objSQLCmd);

                objSQLTrx.Commit();
            }
            catch (Exception ex)
            {
                objSQLTrx.Rollback();
                throw new Exception("Error in SQL:" + ex);
            }
            finally
            {
                objSQLConnection.Close();
            }
        }

        /// <summary>
        /// Update version Financial Data.
        /// </summary>
        /// <param name="token"></param>
        /// <param name="objSQLCmd"></param>
        /// <returns></returns>
        public void UpdateVersionFD(string token, SqlCommand objSQLCmd)
        {
            try
            {
                objSQLCmd.CommandText = "spUpdateVersionFD";
                objSQLCmd.CommandType = CommandType.StoredProcedure;
                objSQLCmd.Parameters.Clear();
                objSQLCmd.Parameters.Add("@TokenVersion", SqlDbType.NVarChar, 50).Value = token;

                objSQLCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw new Exception("Error in SQL:" + ex);
            }
            finally
            {
            }
        }

        /// <summary>
        /// Validate if exist the flag FD into Version Table.
        /// </summary>
        /// <param name="tokenVersion"></param>
        /// <returns></returns>
        public bool ExistVersionFD(string tokenVersion)
        {
            bool existFD = false;

            SqlConnection objSQLConnection = new SqlConnection(strConnectionString);
            try
            {
                SqlDataReader reader;
                SqlCommand objSQLCmd = new SqlCommand();

                objSQLCmd.CommandText = "spValidateFlagFDAndHCD";
                objSQLCmd.CommandType = CommandType.StoredProcedure;

                objSQLCmd.Parameters.Add("@TokenVersion", SqlDbType.NVarChar, 50).Value = tokenVersion;
                objSQLCmd.Parameters.Add("@FlagFD", SqlDbType.Bit).Value = true;
                objSQLCmd.Parameters.Add("@FlagHCD", SqlDbType.Bit).Value = false;

                objSQLCmd.Connection = objSQLConnection;

                objSQLConnection.Open();
                reader = objSQLCmd.ExecuteReader();

                while (reader.Read())
                {
                    //  If the value is greater than zero the FD into Version exist.
                    existFD = reader.GetInt32(0) > 0;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error in SQL:" + ex);
            }
            finally
            {
                objSQLConnection.Close();
            }
            return existFD;
        }

        /// <summary>
        /// Get version Financial entries for send to Anaplan.
        /// </summary>
        /// <param name="cc_code"></param>
        /// <param name="tokenVersion"></param>
        /// <returns></returns>
        public List<FinancialData> GetVersionFDByCCCodeForAnaplan(string cc_code, string tokenVersion)
        {
            var financialDataLst = new List<FinancialData>();
            SqlConnection objSQLConnection = new SqlConnection(strConnectionString);
            var serverDate = DateTime.Now;
            try
            {
                SqlDataReader reader;
                SqlCommand objSQLCmd = new SqlCommand();

                objSQLCmd.CommandText = "sp_GetDataForAnaplan";

                objSQLCmd.CommandType = CommandType.StoredProcedure;

                objSQLCmd.Parameters.Add("@CC_CODE", SqlDbType.NVarChar, 50).Value = cc_code;
                objSQLCmd.Parameters.Add("@TokenVersion", SqlDbType.NVarChar, 50).Value = tokenVersion;

                objSQLCmd.Connection = objSQLConnection;

                objSQLConnection.Open();
                reader = objSQLCmd.ExecuteReader();

                while (reader.Read())
                {
                    var financialData = new FinancialData(reader);
                    financialDataLst.Add(financialData);
                }

                return financialDataLst;
            }
            catch (Exception ex)
            {
                throw new Exception("Error in SQL:" + ex);
            }
            finally
            {
                objSQLConnection.Close();
            }
        }


        /// <summary>
        /// Get version Financial entries or Headcount entries for send to Anaplan in format csv. 
        /// Returns a string array separated by the set limit.
        /// </summary>
        /// <param name="cc_code"></param>
        /// <param name="tokenVersion"></param>
        /// <param name="day"></param>
        /// <param name="year"></param>
        /// <param name="dataType"></param>
        /// <returns></returns>
        public List<string> GetVersionHeadcountDataOrFinancialDataForAnaplan(string cc_code, string tokenVersion, int day,string year,int dataType)
        {
            SqlConnection objSQLConnection = new SqlConnection(strConnectionString);
            StringBuilder sb = new StringBuilder();
            var data = "";
            var header = "";
            var existHeader = false;
            var auxData = "";

            var limit = 500000;
            // Encode the entire string.
            Encoding enc = Encoding.UTF8;
            var dataStringLst = new List<string>();

            try
            {
                SqlDataReader reader;
                SqlCommand objSQLCmd = new SqlCommand();

                objSQLCmd.CommandText = "spGetQueryDataForAnaplan";

                objSQLCmd.CommandType = CommandType.StoredProcedure;

                objSQLCmd.Parameters.Add("@CC_CODE", SqlDbType.NVarChar, 50).Value = cc_code;
                objSQLCmd.Parameters.Add("@TokenVersion", SqlDbType.NVarChar, 50).Value = tokenVersion;
                objSQLCmd.Parameters.Add("@Pivot", SqlDbType.Int).Value = day;
                objSQLCmd.Parameters.Add("@Year", SqlDbType.Char, 4).Value = year;
                objSQLCmd.Parameters.Add("@DataType", SqlDbType.Int).Value = dataType;

                objSQLCmd.Connection = objSQLConnection;

                objSQLConnection.Open();
                reader = objSQLCmd.ExecuteReader();

                while (reader.Read())
                {

                    if (!existHeader)
                    {
                        header = Utils.CSVFormat.GetHeaderCsvFinancialHeadcountDataForAnaPlan(reader);
                        sb.Append(header);
                        existHeader = true;
                    }

                    data = Utils.CSVFormat.GetDataCsvFinancialHeadcountDataForAnaPlan(reader);
                    sb.Append(data);
                    byte[] bytes = enc.GetBytes(sb.ToString());
                    //  If the bytes data is greater than limit.
                    if (bytes.Length > limit)
                    {
                        //  Save the parcial data in the list.
                        dataStringLst.Add(auxData);
                        //  clean buffer.
                        sb = new StringBuilder();
                        auxData = "";
                    }

                    auxData = sb.ToString();
                }
                dataStringLst.Add(auxData);

                return dataStringLst;
            }
            catch (Exception ex)
            {
                throw new Exception("Error in SQL:" + ex);
            }
            finally
            {
                objSQLConnection.Close();
            }
        }

        #endregion

        #region Headcount Data

        /// <summary>
        /// Get version Headcount entries associated to a cost center and version.
        /// </summary>
        /// <param name="cc_code"></param>
        /// <param name="tokenVersion"></param>
        /// <param name="userId"></param> //optional
        /// <returns></returns>
        public List<HeadCountData> GetVersionHCByCCCode(string cc_code, string tokenVersion, string userId = null)
        {
            var headcountDataLst = new List<HeadCountData>();
            SqlConnection objSQLConnection = new SqlConnection(strConnectionString);
            var serverDate = DateTime.Now;
            try
            {
                SqlDataReader reader;
                SqlCommand objSQLCmd = new SqlCommand();

                objSQLCmd.CommandText = "spGetHeadCountDataVersionByCCCode";
                objSQLCmd.CommandType = CommandType.StoredProcedure;

                objSQLCmd.Parameters.Add("@CC_CODE", SqlDbType.NVarChar, 50).Value = cc_code;
                objSQLCmd.Parameters.Add("@TokenVersion", SqlDbType.NVarChar, 50).Value = tokenVersion;

                objSQLCmd.Connection = objSQLConnection;

                objSQLConnection.Open();
                reader = objSQLCmd.ExecuteReader();

                while (reader.Read())
                {
                    var headcountData = new HeadCountData(reader);
                    
                    headcountData.ChangeDate = serverDate;
                    headcountData.ChangeUser = userId;

                    headcountDataLst.Add(headcountData);
                }

                return headcountDataLst;
            }
            catch (Exception ex)
            {
                throw new Exception("Error in SQL:" + ex);
            }
            finally
            {
                objSQLConnection.Close();
            }
        }

        /// <summary>
        /// Insert Headcount entries.
        /// </summary>
        /// <param name="headcountDataLst"></param>
        /// <param name="token"></param>
        public void InsertHeadCountData(List<HeadCountData> headcountDataLst, string token)
        {
            SqlConnection objSQLConnection = new SqlConnection(strConnectionString);
            SqlTransaction objSQLTrx;

            SqlCommand objSQLCmd = objSQLConnection.CreateCommand();
            objSQLConnection.Open();
            objSQLTrx = objSQLConnection.BeginTransaction();

            try
            {
                objSQLCmd.CommandType = CommandType.StoredProcedure;
                objSQLCmd.Transaction = objSQLTrx;

                foreach (var itemHeadCountData in headcountDataLst)
                {
                    objSQLCmd.CommandText = "spInsertHeadcountData";

                    objSQLCmd.Parameters.Clear();
                    objSQLCmd.Parameters.Add("@CC_Code", SqlDbType.NVarChar, 50).Value = itemHeadCountData.CCCode;
                    objSQLCmd.Parameters.Add("@HC_Code", SqlDbType.NVarChar, 50).Value = itemHeadCountData.HCCode;
                    objSQLCmd.Parameters.Add("@Version", SqlDbType.NVarChar, 50).Value = itemHeadCountData.Version;
                    objSQLCmd.Parameters.Add("@Year", SqlDbType.NVarChar, 4).Value = itemHeadCountData.Year;
                    objSQLCmd.Parameters.Add("@PYJan", SqlDbType.Float).Value = itemHeadCountData.PYJan;
                    objSQLCmd.Parameters.Add("@PYFeb", SqlDbType.Float).Value = itemHeadCountData.PYFeb;
                    objSQLCmd.Parameters.Add("@PYMar", SqlDbType.Float).Value = itemHeadCountData.PYMar;
                    objSQLCmd.Parameters.Add("@PYApr", SqlDbType.Float).Value = itemHeadCountData.PYApr;
                    objSQLCmd.Parameters.Add("@PYMay", SqlDbType.Float).Value = itemHeadCountData.PYMay;
                    objSQLCmd.Parameters.Add("@PYJun", SqlDbType.Float).Value = itemHeadCountData.PYJun;
                    objSQLCmd.Parameters.Add("@PYJul", SqlDbType.Float).Value = itemHeadCountData.PYJul;
                    objSQLCmd.Parameters.Add("@PYAug", SqlDbType.Float).Value = itemHeadCountData.PYAug;
                    objSQLCmd.Parameters.Add("@PYSep", SqlDbType.Float).Value = itemHeadCountData.PYSep;
                    objSQLCmd.Parameters.Add("@PYOct", SqlDbType.Float).Value = itemHeadCountData.PYOct;
                    objSQLCmd.Parameters.Add("@PYNov", SqlDbType.Float).Value = itemHeadCountData.NYNov;
                    objSQLCmd.Parameters.Add("@PYDec", SqlDbType.Float).Value = itemHeadCountData.PYDec;
                    objSQLCmd.Parameters.Add("@CYJan", SqlDbType.Float).Value = itemHeadCountData.CYJan;
                    objSQLCmd.Parameters.Add("@CYFeb", SqlDbType.Float).Value = itemHeadCountData.CYFeb;
                    objSQLCmd.Parameters.Add("@CYMar", SqlDbType.Float).Value = itemHeadCountData.CYMar;
                    objSQLCmd.Parameters.Add("@CYApr", SqlDbType.Float).Value = itemHeadCountData.CYApr;
                    objSQLCmd.Parameters.Add("@CYMay", SqlDbType.Float).Value = itemHeadCountData.CYMay;
                    objSQLCmd.Parameters.Add("@CYJun", SqlDbType.Float).Value = itemHeadCountData.CYJun;
                    objSQLCmd.Parameters.Add("@CYJul", SqlDbType.Float).Value = itemHeadCountData.CYJul;
                    objSQLCmd.Parameters.Add("@CYAug", SqlDbType.Float).Value = itemHeadCountData.CYAug;
                    objSQLCmd.Parameters.Add("@CYSep", SqlDbType.Float).Value = itemHeadCountData.CYSep;
                    objSQLCmd.Parameters.Add("@CYOct", SqlDbType.Float).Value = itemHeadCountData.CYOct;
                    objSQLCmd.Parameters.Add("@CYNov", SqlDbType.Float).Value = itemHeadCountData.CYNov;
                    objSQLCmd.Parameters.Add("@CYDec", SqlDbType.Float).Value = itemHeadCountData.CYDec;
                    objSQLCmd.Parameters.Add("@NYJan", SqlDbType.Float).Value = itemHeadCountData.NYJan;
                    objSQLCmd.Parameters.Add("@NYFeb", SqlDbType.Float).Value = itemHeadCountData.NYFeb;
                    objSQLCmd.Parameters.Add("@NYMar", SqlDbType.Float).Value = itemHeadCountData.NYMar;
                    objSQLCmd.Parameters.Add("@NYApr", SqlDbType.Float).Value = itemHeadCountData.NYApr;
                    objSQLCmd.Parameters.Add("@NYMay", SqlDbType.Float).Value = itemHeadCountData.NYMay;
                    objSQLCmd.Parameters.Add("@NYJun", SqlDbType.Float).Value = itemHeadCountData.NYJun;
                    objSQLCmd.Parameters.Add("@NYJul", SqlDbType.Float).Value = itemHeadCountData.NYJul;
                    objSQLCmd.Parameters.Add("@NYAug", SqlDbType.Float).Value = itemHeadCountData.NYAug;
                    objSQLCmd.Parameters.Add("@NYSep", SqlDbType.Float).Value = itemHeadCountData.NYSep;
                    objSQLCmd.Parameters.Add("@NYOct", SqlDbType.Float).Value = itemHeadCountData.NYOct;
                    objSQLCmd.Parameters.Add("@NYNov", SqlDbType.Float).Value = itemHeadCountData.NYNov;
                    objSQLCmd.Parameters.Add("@NYDec", SqlDbType.Float).Value = itemHeadCountData.NYDec;
                    objSQLCmd.Parameters.Add("@ChangeDate", SqlDbType.DateTime).Value = itemHeadCountData.ChangeDate;
                    objSQLCmd.Parameters.Add("@ChangeUser", SqlDbType.NVarChar, 8).Value = itemHeadCountData.ChangeUser;
                    objSQLCmd.Parameters.Add("@TokenVersion", SqlDbType.NVarChar, 50).Value = token;

                    objSQLCmd.ExecuteNonQuery();
                }

                UpdateVersionHCD(token, objSQLCmd);

                objSQLTrx.Commit();
            }
            catch (Exception ex)
            {
                objSQLTrx.Rollback();
                throw new Exception("Error in SQL:" + ex);
            }
            finally
            {
                objSQLConnection.Close();
            }
        }

        /// <summary>
        /// Update version Headcount Data.
        /// </summary>
        /// <param name="token"></param>
        /// <param name="objSQLCmd"></param>
        /// <returns></returns>
        public void UpdateVersionHCD(string token, SqlCommand objSQLCmd)
        {
            try
            {
                objSQLCmd.CommandText = "spUpdateVersionHCD";
                objSQLCmd.CommandType = CommandType.StoredProcedure;

                objSQLCmd.Parameters.Clear();
                objSQLCmd.Parameters.Add("@TokenVersion", SqlDbType.NVarChar, 50).Value = token;

                objSQLCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw new Exception("Error in SQL:" + ex);
            }
            finally
            {
            }
        }

        /// <summary>
        /// Validate if exist the flag HCD into Version Table.
        /// </summary>
        /// <param name="tokenVersion"></param>
        /// <returns></returns>
        public bool ExistVersionHCD(string tokenVersion)
        {
            bool existHCD = false;

            SqlConnection objSQLConnection = new SqlConnection(strConnectionString);
            try
            {
                SqlDataReader reader;
                SqlCommand objSQLCmd = new SqlCommand();

                objSQLCmd.CommandText = "spValidateFlagFDAndHCD";
                objSQLCmd.CommandType = CommandType.StoredProcedure;

                objSQLCmd.Parameters.Add("@TokenVersion", SqlDbType.NVarChar, 50).Value = tokenVersion;
                objSQLCmd.Parameters.Add("@FlagFD", SqlDbType.Bit).Value = false;
                objSQLCmd.Parameters.Add("@FlagHCD", SqlDbType.Bit).Value = true;

                objSQLCmd.Connection = objSQLConnection;

                objSQLConnection.Open();
                reader = objSQLCmd.ExecuteReader();

                while (reader.Read())
                {
                    //  If the value is greater than zero the FD into Version exist.
                    existHCD = reader.GetInt32(0) > 0;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error in SQL:" + ex);
            }
            finally
            {
                objSQLConnection.Close();
            }
            return existHCD;
        }

        /// <summary>
        /// Get version Financial entries for send to Anaplan.
        /// </summary>
        /// <param name="cc_code"></param>
        /// <param name="tokenVersion"></param>
        /// <returns></returns>
        public List<HeadCountData> GetVersionHCDByCCCodeForAnaplan(string cc_code, string tokenVersion)
        {
            var headcountDataLst = new List<HeadCountData>();
            SqlConnection objSQLConnection = new SqlConnection(strConnectionString);
            var serverDate = DateTime.Now;
            try
            {
                SqlDataReader reader;
                SqlCommand objSQLCmd = new SqlCommand();

                objSQLCmd.CommandText = "sp_GetHeadcountDataForAnaplan";

                objSQLCmd.CommandType = CommandType.StoredProcedure;

                objSQLCmd.Parameters.Add("@CC_CODE", SqlDbType.NVarChar, 50).Value = cc_code;
                objSQLCmd.Parameters.Add("@TokenVersion", SqlDbType.NVarChar, 50).Value = tokenVersion;

                objSQLCmd.Connection = objSQLConnection;

                objSQLConnection.Open();
                reader = objSQLCmd.ExecuteReader();

                while (reader.Read())
                {
                    var headcountData = new HeadCountData(reader);
                    headcountDataLst.Add(headcountData);
                }

                return headcountDataLst;
            }
            catch (Exception ex)
            {
                throw new Exception("Error in SQL:" + ex);
            }
            finally
            {
                objSQLConnection.Close();
            }
        }

        #endregion

        #region Cost Element

        /// <summary>
        /// Get the Cost Element data by Cost Center Code.
        /// </summary>
        /// <param name="cc_code"></param>
        /// <returns></returns>
        public List<CostElementMD> GetCEByCCCode(string cc_code)
        {
            var costElementMDLst = new List<CostElementMD>();
            SqlConnection objSQLConnection = new SqlConnection(strConnectionString);
            try
            {
                SqlDataReader reader;
                SqlCommand objSQLCmd = new SqlCommand();

                objSQLCmd.CommandText = "spGetCostElementToAddCostCenter";
                objSQLCmd.CommandType = CommandType.StoredProcedure;

                objSQLCmd.Parameters.Add("@CC_CODE", SqlDbType.NVarChar, 50).Value = cc_code;

                objSQLCmd.Connection = objSQLConnection;

                objSQLConnection.Open();
                reader = objSQLCmd.ExecuteReader();

                while (reader.Read())
                {
                    var costElementMD = new CostElementMD();

                    costElementMD.CECode = reader.GetString(0);
                    costElementMD.Description = reader.GetString(1);
                    costElementMD.CEH1 = reader.GetString(2);
                    costElementMD.CEH2 = reader.GetString(3);
                    costElementMD.CEH3 = reader.GetString(4);
                    costElementMD.CEH4 = reader.GetString(5);

                    costElementMDLst.Add(costElementMD);
                }

                return costElementMDLst;
            }
            catch (Exception ex)
            {
                throw new Exception("Error in SQL:" + ex);
            }
            finally
            {
                objSQLConnection.Close();
            }
        }
        
        #endregion

        #region Version
        
        /// <summary>
        /// Insert the version of the cost center.
        /// </summary>
        /// <param name="version"></param>
        public void InsertVersion(Models.Version version)
        {
            SqlConnection objSQLConnection = new SqlConnection(strConnectionString);

            try
            {
                SqlCommand objSQLCmd = new SqlCommand();
                StringBuilder sb = new StringBuilder();

                objSQLCmd.CommandText = "spInsertVersion";
                objSQLCmd.CommandType = CommandType.StoredProcedure;

                objSQLCmd.Parameters.Add("@CC_Code", SqlDbType.NVarChar, 50).Value = version.CC_CODE;
                objSQLCmd.Parameters.Add("@TokenVersion", SqlDbType.NVarChar, 50).Value = version.TokenVersion;
                objSQLCmd.Parameters.Add("@Timestamp", SqlDbType.DateTime).Value = version.Timestamp;
                objSQLCmd.Parameters.Add("@UserId", SqlDbType.NVarChar, 8).Value = version.UserId;

                objSQLCmd.Connection = objSQLConnection;
                objSQLConnection.Open();

                objSQLCmd.ExecuteNonQuery();

            }
            catch (SqlException ex)
            {
                throw new Exception("Error in SQL:" + ex);
            }
            finally
            {
                objSQLConnection.Close();
            }
        }

        /// <summary>
        /// Get last version associated to a cost center.
        /// </summary>
        /// <param name="cc_code"></param>
        /// <returns></returns>
        public string GetTokenLastVersionByCCCode(string cc_code)
        {
            string token = null;
            SqlConnection objSQLConnection = new SqlConnection(strConnectionString);
            try
            {
                SqlDataReader reader;
                SqlCommand objSQLCmd = new SqlCommand();

                objSQLCmd.CommandText = "spGetTokenLastVersionByCCCode";
                objSQLCmd.CommandType = CommandType.StoredProcedure;

                objSQLCmd.Parameters.Add("@CC_CODE", SqlDbType.NVarChar, 50).Value = cc_code;

                objSQLCmd.Connection = objSQLConnection;

                objSQLConnection.Open();
                reader = objSQLCmd.ExecuteReader();

                while (reader.Read())
                {
                    token = reader.GetString(0);
                }

                return token;
            }
            catch (Exception ex)
            {
                throw new Exception("Error in SQL:" + ex);
            }
            finally
            {
                objSQLConnection.Close();
            }
        }

        /// <summary>
        /// Update flag timestamp send Anaplan.
        /// </summary>
        /// <param name="token"></param>
        /// <param name="timestamp"></param>
        public void UpdateVersionFlagAnaplan(string token, DateTime timestamp)
        {
            SqlConnection objSQLConnection = new SqlConnection(strConnectionString);
            try
            {
                SqlCommand objSQLCmd = new SqlCommand();

                objSQLCmd.CommandText = "spUpdateAnaplanTimestamp";
                objSQLCmd.CommandType = CommandType.StoredProcedure;

                objSQLCmd.Parameters.Add("@TokenVersion", SqlDbType.NVarChar, 50).Value = token;
                objSQLCmd.Parameters.Add("@Timestamp", SqlDbType.DateTime).Value = timestamp;

                objSQLCmd.Connection = objSQLConnection;
                objSQLConnection.Open();

                objSQLCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw new Exception("Error in SQL:" + ex);
            }
            finally
            {
                objSQLConnection.Close();
            }
        }

        /// <summary>
        /// Check if exist flag Anaplan timestamp checked.
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        public bool CheckAnaplanTimestamp(string token)
        {
            SqlConnection objSQLConnection = new SqlConnection(strConnectionString);
            var exist = false;

            try
            {
                SqlDataReader reader;
                SqlCommand objSQLCmd = new SqlCommand();

                objSQLCmd.CommandText = "spCheckAnaplanTimestamp";
                objSQLCmd.CommandType = CommandType.StoredProcedure;

                objSQLCmd.Parameters.Add("@TokenVersion", SqlDbType.NVarChar, 50).Value = token;

                objSQLCmd.Connection = objSQLConnection;

                objSQLConnection.Open();
                reader = objSQLCmd.ExecuteReader();

                while (reader.Read())
                {
                    //  If the value is greater than zero the user and code exist
                    exist = reader.GetInt32(0) > 0;
                }

                return exist;
            }
            catch (Exception ex)
            {
                throw new Exception("Error in SQL:" + ex);
            }
            finally
            {
                objSQLConnection.Close();
            }
        }

        /// <summary>
        /// Update flag version Headcount Data from Anaplan.
        /// </summary>
        /// <param name="token"></param>
        public void UpdateVersionHCDAnaplan(string token)
        {
            SqlConnection objSQLConnection = new SqlConnection(strConnectionString);
            SqlTransaction objSQLTrx;

            SqlCommand objSQLCmd = objSQLConnection.CreateCommand();
            objSQLConnection.Open();
            objSQLTrx = objSQLConnection.BeginTransaction();

            try
            {
                objSQLCmd.CommandType = CommandType.StoredProcedure;
                objSQLCmd.Transaction = objSQLTrx;

                UpdateVersionHCD(token, objSQLCmd);

                objSQLTrx.Commit();
            }
            catch (Exception ex)
            {
                objSQLTrx.Rollback();
                throw new Exception("Error in SQL:" + ex);
            }
            finally
            {
                objSQLConnection.Close();
            }
        }

        /// <summary>
        /// Update flag version Financial Data from Anaplan.
        /// </summary>
        /// <param name="token"></param>
        public void UpdateVersionFDAnaplan(string token)
        {
            SqlConnection objSQLConnection = new SqlConnection(strConnectionString);
            SqlTransaction objSQLTrx;

            SqlCommand objSQLCmd = objSQLConnection.CreateCommand();
            objSQLConnection.Open();
            objSQLTrx = objSQLConnection.BeginTransaction();

            try
            {
                objSQLCmd.CommandType = CommandType.StoredProcedure;
                objSQLCmd.Transaction = objSQLTrx;

                UpdateVersionFD(token, objSQLCmd);

                objSQLTrx.Commit();
            }
            catch (Exception ex)
            {
                objSQLTrx.Rollback();
                throw new Exception("Error in SQL:" + ex);
            }
            finally
            {
                objSQLConnection.Close();
            }
        }
        
        #endregion

        #region WSLog
        
        /// <summary>
        /// Insert the web server log.
        /// </summary>
        /// <param name="wsLog"></param>
        public void InsertWSLog(WSLog wsLog)
        {
            SqlConnection objSQLConnection = new SqlConnection(strConnectionString);
            try
            {
                SqlCommand objSQLCmd = new SqlCommand();
                StringBuilder sb = new StringBuilder();

                objSQLCmd.CommandText = "spInsertLogWS";
                objSQLCmd.CommandType = CommandType.StoredProcedure;

                objSQLCmd.Parameters.Add("@UserId", SqlDbType.NVarChar, 8).Value = wsLog.UserId;
                objSQLCmd.Parameters.Add("@Method", SqlDbType.NVarChar, 10).Value = wsLog.Method;
                objSQLCmd.Parameters.Add("@Url", SqlDbType.NVarChar, 50).Value = wsLog.Url;
                objSQLCmd.Parameters.Add("@StatusCode", SqlDbType.Int).Value = wsLog.StatusCode;
                objSQLCmd.Parameters.Add("@WebServerMsg", SqlDbType.NVarChar, 500).Value = wsLog.WebServerMessage;
                objSQLCmd.Parameters.Add("@AccessDate", SqlDbType.DateTime).Value = wsLog.AccessDate;

                objSQLCmd.Connection = objSQLConnection;
                objSQLConnection.Open();

                objSQLCmd.ExecuteNonQuery();

            }
            catch (SqlException ex)
            {
                throw new Exception("Error in SQL:" + ex);
            }
            finally
            {
                objSQLConnection.Close();
            }
        }

        #endregion
        
        #region Bulk Insert

        /// <summary>
        /// Bulk insert from list.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        /// <param name="TableName"></param>
        public void InsertData<T>(List<T> list, string TableName)
        {
            DataTable dt = new DataTable("MyTable");
            
            try
            {
                var bulkCopyTimeOut = Utils.Configuration.GetIntConfiguration("bulkCopyTimeOut");
                //clsBulkOperation blk = new clsBulkOperation();
                dt = ConvertToDataTable(list);
                //ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.PerUserRoamingAndLocal);

                //using (SqlBulkCopy bulkcopy = new SqlBulkCopy(strConnectionString,SqlBulkCopyOptions.CheckConstraints))
                using (SqlBulkCopy bulkcopy = new SqlBulkCopy(strConnectionString))
                {
                    bulkcopy.BulkCopyTimeout = bulkCopyTimeOut;
                    bulkcopy.DestinationTableName = TableName;
                    bulkcopy.WriteToServer(dt);
                }
            }catch(Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Convert list into data table.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="data"></param>
        /// <returns></returns>
        public static DataTable ConvertToDataTable<T>(IList<T> data)
        {
            DataTable table = new DataTable();

            try
            {
                PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(T));
                //  Create table column from properties of object class.
                foreach (PropertyDescriptor prop in properties)
                    table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
                //  Create new rows from table column and add the data of the list.
                foreach (T item in data)
                {
                    DataRow row = table.NewRow();
                    foreach (PropertyDescriptor prop in properties)
                        row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                    table.Rows.Add(row);
                }
            }catch(Exception ex)
            {
                throw ex;
            }

            return table;
        }

        #endregion
    }
}