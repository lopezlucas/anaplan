﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Threading;

namespace ABWS.BusinessLogic
{
    public class AnaplanBL
    {
        public const int FINANCIAL_DATA = 0;
        public const int HEADCOUNT_DATA = 1;
        
        /// <summary>
        /// Upload to Anaplan the Financial Data and Headcount Data.
        /// </summary>
        /// <param name="cc_code"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public bool UploadFinacialDataAndHeadcountData(string cc_code, string token)
        {
            var result = new HttpResponseMessage();
            var index = 0;
            var isOk = true;

            try
            {
                var db = new DBAccess.DBDataAccess();

                string importFinancialID = Utils.Configuration.GetConfiguration("importFinancialID");
                string importHeadcountID = Utils.Configuration.GetConfiguration("importHeadcountID");
                string importFileFinancialID = Utils.Configuration.GetConfiguration("importFileFinancialID");
                string importFileHeadcountID = Utils.Configuration.GetConfiguration("importFileHeadcountID");

                int day = Utils.Configuration.GetIntConfiguration("pivotDay");
                string year = Utils.Configuration.GetConfiguration("year");

                //  Get the Cost Center List by cost center code.
                var costCenterFDList = db.GetVersionFDByCCCodeForAnaplan(cc_code, token);
                // Convert the Cost Center List to csv format.
                var retValFDLst = Utils.CSVFormat.GetFinancialDataForAnaplan(costCenterFDList);

                //List<string> retValFDLst = db.GetVersionHeadcountDataOrFinancialDataForAnaplan(cc_code, token, day, year, FINANCIAL_DATA);

                //  Send to anaplan the cost center data part by part.
                foreach (var financialData in retValFDLst)
                {
                    isOk = UploadData(financialData, index, importFinancialID, importFileFinancialID);

                    if (!isOk)
                        break;

                    index++;
                }

                //  If data from financal data was successfully sent.
                if (isOk) {
                    //  Reset the index for Headcount Data.
                    index = 0;
                    //  Get the Headcount Data List by cost center and token.
                    var headcountDataLst = db.GetVersionHCDByCCCodeForAnaplan(cc_code, token);
                    // Convert the Headcount Data List to csv format.
                    var retValHCDLst = Utils.CSVFormat.GetHeadcountDataForAnaplan(headcountDataLst);

                    //List<string> retValHCDLst = db.GetVersionHeadcountDataOrFinancialDataForAnaplan(cc_code, token, day, year, HEADCOUNT_DATA);

                    //  Send to anaplan the Headcount Data part by part.
                    foreach (var headcountData in retValHCDLst)
                    {
                        isOk = UploadData(headcountData, index, importHeadcountID, importFileHeadcountID);

                        if (!isOk)
                            break;

                        index++;
                    }
                }

                //  If the operation is correct update flag version of Anaplan.
                if (isOk)
                {
                    var timestamp = DateTime.Now;
                    db.UpdateVersionFlagAnaplan(token, timestamp);
                }
            }catch(Exception ex)
            {
                throw ex;
            }

            return isOk;
        }

        /// <summary>
        ///  Upload data to Anaplan.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="chunk"></param>
        /// <param name="importID"></param>
        /// <param name="fileID"></param>
        /// <returns></returns>
        public bool UploadData(string data,int chunk,string importID, string fileID)
        {
            var method = System.Reflection.MethodBase.GetCurrentMethod().Name;
            var httpResponseMessage = new HttpResponseMessage();
            var isOk = true;
            var userId = "0";

            try
            {
                string urlUpload = Utils.Configuration.GetUrlUploadDowload(chunk, fileID);
                string urlTaskExec = Utils.Configuration.GetUrlTaskExec("imports", importID);

                userId = Utils.Configuration.GetConfiguration("userAnaplan");

                ServicePointManager.ServerCertificateValidationCallback = delegate (Object obj, X509Certificate certificate, X509Chain chain, SslPolicyErrors errors)
                {
                    return true;
                };

                string certificateAuthorizationHeader = Utils.Certificate.GetCertificateAuthorizationHeader();

                try
                {
                    httpResponseMessage = PutToUpload(certificateAuthorizationHeader, data, urlUpload);
                }catch(Exception ex)
                {
                    isOk = false;
                }

                if (isOk)
                {
                    //  Note that a response of "204 No Content" indicates success.
                    if (httpResponseMessage.StatusCode == HttpStatusCode.NoContent)
                    {
                        //  Generate task.
                        isOk = PostToGenerateTask(certificateAuthorizationHeader, urlTaskExec);
                    }
                    else
                    {
                        isOk = false;
                        Utils.WSLogHelper.AddWSLog(userId, method, urlTaskExec, (int)httpResponseMessage.StatusCode, "");

                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return isOk;
        }

        /// <summary>
        /// Download from Anaplan the Financial Data and Headcount Data.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="tokenVersion"></param>
        /// <returns></returns>
        public bool DownloadFinacialDataAndHeadcountData(string userId, string tokenVersion)
        {
            var result = new HttpResponseMessage();
            var isOk = true;

            try
            {
                var db = new DBAccess.DBDataAccess();

                string exportFinancialID = Utils.Configuration.GetConfiguration("exportFinancialID");
                string exportHeadcountID = Utils.Configuration.GetConfiguration("exportHeadcountID");
                string exportFileFinancialID = Utils.Configuration.GetConfiguration("exportFileFinancialID");
                string exportFileHeadcountID = Utils.Configuration.GetConfiguration("exportFileHeadcountID");
                
                //  Download and insert into database the Financial Data.
                result = DownloadData(userId, exportFinancialID, exportFileFinancialID, tokenVersion, FINANCIAL_DATA);
                
                if(result.StatusCode == HttpStatusCode.OK)
                {
                    //  Download and insert into database the Headcount Data.
                    result = DownloadData(userId, exportHeadcountID, exportFileHeadcountID, tokenVersion, HEADCOUNT_DATA);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return isOk;
        }

        /// <summary>
        /// Download data from Anaplan.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="exportId"></param>
        /// <param name="fileId"></param>
        /// <param name="tokenVersion"></param>
        /// <param name="dataType"></param>
        /// <returns></returns>
        public HttpResponseMessage DownloadData(string userId,string exportId,string fileId,string tokenVersion, int dataType)
        {
            var httpResponseMessage = new HttpResponseMessage();
            httpResponseMessage.StatusCode = HttpStatusCode.NoContent;

            try
            {
                string urlGetChunk = Utils.Configuration.GetUrlUploadDowload(null, fileId);
                string urlTaskExec = Utils.Configuration.GetUrlTaskExec("exports", exportId);

                ServicePointManager.ServerCertificateValidationCallback = delegate (Object obj, X509Certificate certificate, X509Chain chain, SslPolicyErrors errors)
                {
                    return true;
                };

                string certificateAuthorizationHeader = Utils.Certificate.GetCertificateAuthorizationHeader();

                //  Generate task.
                bool isOk = PostToGenerateTask(certificateAuthorizationHeader, urlTaskExec);

                if (isOk)
                {
                    //  Get the chunk count from anaplan for download data.
                    var chunkCount = GetChunkToDownload(certificateAuthorizationHeader, urlGetChunk);

                    //  Download data from anaplan and save into database.
                    httpResponseMessage = SaveDataFromAnaplan(certificateAuthorizationHeader, urlGetChunk, chunkCount, userId, tokenVersion, dataType);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return httpResponseMessage;
        }

        /// <summary>
        /// PUT to Anaplan for the upload data.
        /// </summary>
        /// <param name="certificateAuthorizationHeader"></param>
        /// <param name="data"></param>
        /// <param name="urlUpload"></param>
        /// <returns></returns>
        private HttpResponseMessage PutToUpload(string certificateAuthorizationHeader, string data, string urlUpload)
        {
            var httpResponseMessage = new HttpResponseMessage();
            
            try
            {
                var httpClient = Utils.Configuration.GetHttpClientContentTypeJsonPut(certificateAuthorizationHeader);

                StringContent content = new StringContent(data, System.Text.Encoding.Default, "application/octet-stream");
                var timeout = Utils.Configuration.GetIntConfiguration("timeOutAnaplan"); 
                httpClient.Timeout = new TimeSpan(0, 0, timeout);
                //  Call Anaplan with PUT. Content to upload the data.
                httpResponseMessage = httpClient.PutAsync(urlUpload, content).Result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return httpResponseMessage;
        }

        /// <summary>
        /// POST to Anaplan for the generate task ID.
        /// </summary>
        /// <param name="certificateAuthorizationHeader"></param>
        /// <param name="urlTaskExec"></param>
        /// <returns></returns>
        private bool PostToGenerateTask(string certificateAuthorizationHeader, string urlTaskExec)
        {
            var method = System.Reflection.MethodBase.GetCurrentMethod().Name;
            var httpResponseMessage = new HttpResponseMessage();
            var isOk = true;
            var userId = "0";

            StringContent contentExec = new StringContent("{  \"localeName\": \"en_US\"}", System.Text.Encoding.Default, "application/json");

            try
            {
                var httpClient = Utils.Configuration.GetHttpClientAuthorizationHeader(certificateAuthorizationHeader);
                var timeout = Utils.Configuration.GetIntConfiguration("timeOutAnaplan");

                userId = Utils.Configuration.GetConfiguration("userAnaplan");

                httpClient.Timeout = new TimeSpan(0, 0, timeout);

                try
                {
                    //  Call Anaplan with POST.
                    httpResponseMessage = httpClient.PostAsync(urlTaskExec, contentExec).Result;
                }catch(Exception ex)
                {
                    isOk = false;
                }

                if (httpResponseMessage.StatusCode == HttpStatusCode.OK)
                {
                    //  If execute correct the POST to Anaplan.
                    if (isOk)
                    {
                        //  The result POST to anaplan get the task ID.
                        var responseData = httpResponseMessage.Content.ReadAsStringAsync().Result;
                        //  Get json object result.
                        dynamic taskResult = JsonConvert.DeserializeObject<dynamic>(responseData);
                        //  Generate the new URL with the task ID to get the task data.
                        var urlTaskExecWithId = urlTaskExec + taskResult.taskId;

                        int statusTaskExportAttempts = Utils.Configuration.GetIntConfiguration("statusTaskExportAttempts");
                        int statusTaskExportWaitTime = Utils.Configuration.GetIntConfiguration("statusTaskExportWaitTime"); ;

                        //  Make the attempts to get the result of the task.
                        for (var i = 0; i <= statusTaskExportAttempts; i++)
                        {
                            //  The first time does not wait.
                            if (i > 0)
                                Thread.Sleep(statusTaskExportWaitTime);
                            //  Get the task state.
                            string taskState = GetToValidateTaskStatus(certificateAuthorizationHeader, urlTaskExecWithId);

                            //  If the task is complete it leaves the loop.
                            if (taskState.Equals("COMPLETE"))
                                break;
                        }
                    }
                }
                else
                {
                    Utils.WSLogHelper.AddWSLog(userId, method, urlTaskExec, (int)httpResponseMessage.StatusCode, "");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return isOk;
        }

        /// <summary>
        /// GET to Anaplan for get task status.
        /// </summary>
        /// <param name="certificateAuthorizationHeader"></param>
        /// <param name="urlTaskExecWithId"></param>
        /// <returns></returns>
        private string GetToValidateTaskStatus(string certificateAuthorizationHeader, string urlTaskExecWithId)
        {
            string taskState = "INCOMPLETE";
            
            try
            {
                var httpClient = Utils.Configuration.GetHttpClientAuthorizationHeader(certificateAuthorizationHeader);

                //  Call Anaplan with GET. Get the task data result.
                var responseData = httpClient.GetAsync(urlTaskExecWithId).Result.Content.ReadAsStringAsync().Result;

                //  Get json object result.
                dynamic taskResult = JsonConvert.DeserializeObject<dynamic>(responseData);

                //  Get task state from json object.
                taskState = taskResult.taskState;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return taskState;
        }

        /// <summary>
        /// GET to Anaplan for get count chunk.
        /// </summary>
        /// <param name="certificateAuthorizationHeader"></param>
        /// <param name="urlGetChunk"></param>
        /// <returns></returns>
        private int GetChunkToDownload(string certificateAuthorizationHeader, string urlGetChunk)
        {
            var chunkCount = 0;
            
            try
            {
                var httpClient = Utils.Configuration.GetHttpClientAuthorizationHeader(certificateAuthorizationHeader);
                var timeout = Utils.Configuration.GetIntConfiguration("timeOutAnaplan");
                httpClient.Timeout = new TimeSpan(0, 0, timeout);
                //  Call Anaplan with GET. Get the task result.
                var responseData = httpClient.GetAsync(urlGetChunk).Result.Content.ReadAsStringAsync().Result;
                //  Get json object result.
                dynamic chunkResult = JsonConvert.DeserializeObject<dynamic>(responseData);
                //  Get chunk count from json object.
                chunkCount = chunkResult.Count;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return chunkCount;
        }

        /// <summary>
        /// Save data into data base from Anaplan for each chunk.
        /// </summary>
        /// <param name="certificateAuthorizationHeader"></param>
        /// <param name="urlGetChunk"></param>
        /// <param name="chunkCount"></param>
        /// <param name="userId"></param>
        /// <param name="tokenVersion"></param>
        /// <param name="dataType"></param>
        /// <returns></returns>
        private HttpResponseMessage SaveDataFromAnaplan(string certificateAuthorizationHeader, string urlGetChunk, int chunkCount, string userId,string tokenVersion, int dataType)
        {
            var httpResponseMessage = new HttpResponseMessage();
            
            try
            {
                var db = new DBAccess.DBDataAccess();
                var financialDataGlobalLst = new List<Models.FinancialDataAnaplan>();
                var headcountDataGlobalLst = new List<Models.HeadcountDataAnaplan>();
                var costCenterGlobalLst = new List<string>();

                //  run each chunk to be able to download it.
                for (var i = 0; i < chunkCount; i++)
                {
                    var httpClient = Utils.Configuration.GetHttpClientContentTypeOctetStream(certificateAuthorizationHeader);
                    var timeout = Utils.Configuration.GetIntConfiguration("timeOutAnaplan");
                    httpClient.Timeout = new TimeSpan(0, 0, timeout);
                    //  Set the URL to be able to download a specific chunk.
                    var urlDownload = urlGetChunk + i;
                    //  Call Anaplan with GET. Get the data result in format csv.
                    httpResponseMessage = httpClient.GetAsync(urlDownload).Result;
                    // String of Financial Data.
                    string dataDownloaded = httpResponseMessage.Content.ReadAsStringAsync().Result;

                    switch (dataType)
                    {
                        case FINANCIAL_DATA:
                            //  Convert the data csv to Financial data list and insert the token version.
                            var financialDataLst = Utils.CSVFormat.GetFinancialDataAnaplanListFromText(dataDownloaded, userId, tokenVersion, costCenterGlobalLst);
                            financialDataGlobalLst.AddRange(financialDataLst);
                            break;
                        case HEADCOUNT_DATA:
                            //  Convert the data csv to Headcount Data list.
                            var headcountDataLst = Utils.CSVFormat.GetHeadcountDataAnaplanListFromText(dataDownloaded, userId, tokenVersion);
                            headcountDataGlobalLst.AddRange(headcountDataLst);
                            break;
                        default:
                            break;
                    }
                }

                switch (dataType)
                {
                    case FINANCIAL_DATA:
                        //  Insert into data base the Financial Data.
                        db.InsertData(financialDataGlobalLst, "FinancialData");
                        //  Update the flag of Financial Data.
                        db.UpdateVersionFDAnaplan(tokenVersion);
                        break;
                    case HEADCOUNT_DATA:
                        //  Insert into data base the Headcount Data.
                        db.InsertData(headcountDataGlobalLst, "HeadcountData");
                        //  Update the flag of Headcount Data.
                        db.UpdateVersionHCDAnaplan(tokenVersion);
                        break;
                    default:
                        break;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return httpResponseMessage;
        }
    }
}