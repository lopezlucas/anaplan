﻿using System;
using System.Data.SqlClient;

namespace ABWS.Models
{
    public class FinancialData
    {
        public FinancialData() { }

        /// <summary>
        /// Map the return data from the data base to object class.
        /// </summary>
        /// <param name="reader"></param>
        public FinancialData(SqlDataReader reader)
        {
            var costElement = new CostElementMD();

            for (var i = 0; i < reader.FieldCount; i++)
            {
                switch (reader.GetName(i))
                {
                    case "CC_CODE":
                        CCCode = reader.GetString(i);
                        break;
                    case "CE_CODE":
                        costElement.CECode = reader.GetString(i);
                        break;
                    case "Year":
                        Year = reader.GetString(i);
                        break;
                    case "Version":
                        Version = reader.GetString(i);
                        break;
                    case "PYJan":
                        PYJan = reader.GetDouble(i);
                        break;
                    case "PYFeb":
                        PYFeb = reader.GetDouble(i);
                        break;
                    case "PYMar":
                        PYMar = reader.GetDouble(i);
                        break;
                    case "PYApr":
                        PYApr = reader.GetDouble(i);
                        break;
                    case "PYMay":
                        PYMay = reader.GetDouble(i);
                        break;
                    case "PYJun":
                        PYJun = reader.GetDouble(i);
                        break;
                    case "PYJul":
                        PYJul = reader.GetDouble(i);
                        break;
                    case "PYAug":
                        PYAug = reader.GetDouble(i);
                        break;
                    case "PYSep":
                        PYSep = reader.GetDouble(i);
                        break;
                    case "PYOct":
                        PYOct = reader.GetDouble(i);
                        break;
                    case "PYNov":
                        PYNov = reader.GetDouble(i);
                        break;
                    case "PYDec":
                        PYDec = reader.GetDouble(i);
                        break;
                    case "CYJan":
                        CYJan = reader.GetDouble(i);
                        break;
                    case "CYFeb":
                        CYFeb = reader.GetDouble(i);
                        break;
                    case "CYMar":
                        CYMar = reader.GetDouble(i);
                        break;
                    case "CYApr":
                        CYApr = reader.GetDouble(i);
                        break;
                    case "CYMay":
                        CYMay = reader.GetDouble(i);
                        break;
                    case "CYJun":
                        CYJun = reader.GetDouble(i);
                        break;
                    case "CYJul":
                        CYJul = reader.GetDouble(i);
                        break;
                    case "CYAug":
                        CYAug = reader.GetDouble(i);
                        break;
                    case "CYSep":
                        CYSep = reader.GetDouble(i);
                        break;
                    case "CYOct":
                        CYOct = reader.GetDouble(i);
                        break;
                    case "CYNov":
                        CYNov = reader.GetDouble(i);
                        break;
                    case "CYDec":
                        CYDec = reader.GetDouble(i);
                        break;
                    case "NYJan":
                        NYJan = reader.GetDouble(i);
                        break;
                    case "NYFeb":
                        NYFeb = reader.GetDouble(i);
                        break;
                    case "NYMar":
                        NYMar = reader.GetDouble(i);
                        break;
                    case "NYApr":
                        NYApr = reader.GetDouble(i);
                        break;
                    case "NYMay":
                        NYMay = reader.GetDouble(i);
                        break;
                    case "NYJun":
                        NYJun = reader.GetDouble(i);
                        break;
                    case "NYJul":
                        NYJul = reader.GetDouble(i);
                        break;
                    case "NYAug":
                        NYAug = reader.GetDouble(i);
                        break;
                    case "NYSep":
                        NYSep = reader.GetDouble(i);
                        break;
                    case "NYOct":
                        NYOct = reader.GetDouble(i);
                        break;
                    case "NYNov":
                        NYNov = reader.GetDouble(i);
                        break;
                    case "NYDec":
                        NYDec = reader.GetDouble(i);
                        break;
                    case "ChangeDate":
                        ChangeDate = reader.GetDateTime(i);
                        break;
                    case "ChangeUser":
                        ChangeUser = reader.GetString(i);
                        break;
                    case "TokenVersion":
                        TokenVersion = reader.GetString(i);
                        break;
                    case "CEH1":
                        costElement.CEH1 = reader.GetString(i);
                        break;
                    case "CEH2":
                        costElement.CEH2 = reader.GetString(i);
                        break;
                    case "CEH3":
                        costElement.CEH3 = reader.GetString(i);
                        break;
                    case "CEH4":
                        costElement.CEH4 = reader.GetString(i);
                        break;
                    case "Description":
                        costElement.Description = reader.GetString(i);
                        break;
                    default:
                        break;
                }
            }
            CostElementMD = costElement;
        }
        public string CCCode { get; set; }
        public CostElementMD CostElementMD { get; set; }
        public string Version { get; set; }
        public string Year { get; set; }
        public double PYJan { get; set; }
        public double PYFeb { get; set; }
        public double PYMar { get; set; }
        public double PYApr { get; set; }
        public double PYMay { get; set; }
        public double PYJun { get; set; }
        public double PYJul { get; set; }
        public double PYAug { get; set; }
        public double PYSep { get; set; }
        public double PYOct { get; set; }
        public double PYNov { get; set; }
        public double PYDec { get; set; }
        public double CYJan { get; set; }
        public double CYFeb { get; set; }
        public double CYMar { get; set; }
        public double CYApr { get; set; }
        public double CYMay { get; set; }
        public double CYJun { get; set; }
        public double CYJul { get; set; }
        public double CYAug { get; set; }
        public double CYSep { get; set; }
        public double CYOct { get; set; }
        public double CYNov { get; set; }
        public double CYDec { get; set; }
        public double NYJan { get; set; }
        public double NYFeb { get; set; }
        public double NYMar { get; set; }
        public double NYApr { get; set; }
        public double NYMay { get; set; }
        public double NYJun { get; set; }
        public double NYJul { get; set; }
        public double NYAug { get; set; }
        public double NYSep { get; set; }
        public double NYOct { get; set; }
        public double NYNov { get; set; }
        public double NYDec { get; set; }
        public DateTime ChangeDate { get; set; }
        public string ChangeUser { get; set; }
        public string TokenVersion { get; set; }
    }
}