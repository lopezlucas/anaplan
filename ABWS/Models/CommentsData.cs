﻿
namespace ABWS.Models
{
    public class CommentsData
    {
        public string CCCode { get; set; }
        public string CECode { get; set; }
        public string Version { get; set; }
        public string Comment { get; set; }
    }
}