﻿using System;

namespace ABWS.Models
{
    public class Version
    {
        public Version() { }
        
        public Version(string ccCode,string tokenVersion,string userId,DateTime timestamp)
        {
            this.CC_CODE = ccCode;
            this.TokenVersion = tokenVersion;
            this.Timestamp = timestamp;
            this.UserId = userId;
        }
        
        public string CC_CODE { get; set; }
        public string TokenVersion { get; set; }
        public bool ExistFD { get; set; }
        public bool ExistHCD { get;set; }
        public string UserId { get; set; }
        public DateTime Timestamp { get; set; }
        public DateTime AnaplanTimestamp { get; set; }
    }
}