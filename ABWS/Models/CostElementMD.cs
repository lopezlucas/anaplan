﻿using System.Data.SqlClient;

namespace ABWS.Models
{
    public class CostElementMD
    {
        public CostElementMD() { }

        /// <summary>
        /// Map the return data from the data base to object class.
        /// </summary>
        /// <param name="reader"></param>
        public CostElementMD(SqlDataReader reader)
        {

            for (var i = 0; i < reader.FieldCount; i++)
            {
                switch (reader.GetName(i))
                {
                    case "CC_CODE":
                        CECode = reader.GetString(i);
                        break;
                    case "Description":
                        Description = reader.GetString(i);
                        break;
                    case "CEH1":
                        CEH1 = reader.GetString(i);
                        break;
                    case "CEH2":
                        CEH2 = reader.GetString(i);
                        break;
                    case "CEH3":
                        CEH3 = reader.GetString(i);
                        break;
                    case "CEH4":
                        CEH4 = reader.GetString(i);
                        break;
                    case "CE_ORDER":
                        CE_Order = reader.GetInt32(i);
                        break;
                    default:
                        break;
                }
            }
        }
        public string CECode { get; set; }
        public string Description { get; set; }
        public string CEH1 { get; set; }
        public string CEH2 { get; set; }
        public string CEH3 { get; set; }
        public string CEH4 { get; set; }
        public int CE_Order { get; set; }
    }
}