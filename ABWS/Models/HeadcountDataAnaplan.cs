﻿using System;

namespace ABWS.Models
{
    public class HeadcountDataAnaplan
    {
        public HeadcountDataAnaplan() { }

        public string CC_CODE { get; set; }
        public string HC_CODE { get; set; }
        public string Version { get; set; }
        public string Year { get; set; }
        public double PYJan { get; set; }
        public double PYFeb { get; set; }
        public double PYMar { get; set; }
        public double PYApr { get; set; }
        public double PYMay { get; set; }
        public double PYJun { get; set; }
        public double PYJul { get; set; }
        public double PYAug { get; set; }
        public double PYSep { get; set; }
        public double PYOct { get; set; }
        public double PYNov { get; set; }
        public double PYDec { get; set; }
        public double CYJan { get; set; }
        public double CYFeb { get; set; }
        public double CYMar { get; set; }
        public double CYApr { get; set; }
        public double CYMay { get; set; }
        public double CYJun { get; set; }
        public double CYJul { get; set; }
        public double CYAug { get; set; }
        public double CYSep { get; set; }
        public double CYOct { get; set; }
        public double CYNov { get; set; }
        public double CYDec { get; set; }
        public double NYJan { get; set; }
        public double NYFeb { get; set; }
        public double NYMar { get; set; }
        public double NYApr { get; set; }
        public double NYMay { get; set; }
        public double NYJun { get; set; }
        public double NYJul { get; set; }
        public double NYAug { get; set; }
        public double NYSep { get; set; }
        public double NYOct { get; set; }
        public double NYNov { get; set; }
        public double NYDec { get; set; }
        public DateTime ChangeDate { get; set; }
        public string ChangeUser { get; set; }
        public string TokenVersion { get; set; }
    }
}