﻿using System;

namespace ABWS.Models
{
    public class WSLog
    {
        public string UserId { get; set; }
        public string Method { get; set; }
        public string Url { get; set; }
        public int StatusCode { get; set; }
        public string WebServerMessage { get; set; }
        public DateTime AccessDate { get; set; }
    }
}