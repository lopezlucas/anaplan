﻿using Microsoft.Web.Http.Routing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Routing;
using System.Web.Routing;

namespace ABWS
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            var constraintResolver = new DefaultInlineConstraintResolver() { ConstraintMap = { ["apiVersion"] = typeof(ApiVersionRouteConstraint) } };

            // reporting api versions will return the headers "api-supported-versions" and "api-deprecated-versions"
            
            GlobalConfiguration.Configuration.AddApiVersioning(o => o.ReportApiVersions = true);
            GlobalConfiguration.Configuration.MapHttpAttributeRoutes(constraintResolver);

            GlobalConfiguration.Configure(WebApiConfig.Register);

        }
    }
}
