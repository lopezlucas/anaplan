﻿using System;
using System.Data.SqlClient;

namespace ABWS.Models
{
    public class FinancialData
    {
        public FinancialData() { }

        /// <summary>
        /// Map the return data from the data base to object class
        /// </summary>
        /// <param name="reader"></param>
        public FinancialData(SqlDataReader reader)
        {
            var costElement = new CostElementMD();

            for (var i = 0; i < reader.FieldCount; i++)
            {
                switch (reader.GetName(i))
                {
                    case "CC_CODE":
                        CCCode = reader.GetString(i);
                        break;
                    case "CE_CODE":
                        costElement.CECode = reader.GetString(i);
                        break;
                    case "Year":
                        Year = reader.GetString(i);
                        break;
                    case "Version":
                        Version = reader.GetString(i);
                        break;
                    case "PYJan":
                        PYJan = reader.GetDecimal(i);
                        break;
                    case "PYFeb":
                        PYFeb = reader.GetDecimal(i);
                        break;
                    case "PYMar":
                        PYMar = reader.GetDecimal(i);
                        break;
                    case "PYApr":
                        PYApr = reader.GetDecimal(i);
                        break;
                    case "PYMay":
                        PYMay = reader.GetDecimal(i);
                        break;
                    case "PYJun":
                        PYJun = reader.GetDecimal(i);
                        break;
                    case "PYJul":
                        PYJul = reader.GetDecimal(i);
                        break;
                    case "PYAug":
                        PYAug = reader.GetDecimal(i);
                        break;
                    case "PYSep":
                        PYSep = reader.GetDecimal(i);
                        break;
                    case "PYOct":
                        PYOct = reader.GetDecimal(i);
                        break;
                    case "PYNov":
                        PYNov = reader.GetDecimal(i);
                        break;
                    case "PYDec":
                        PYDec = reader.GetDecimal(i);
                        break;
                    case "CYJan":
                        CYJan = reader.GetDecimal(i);
                        break;
                    case "CYFeb":
                        CYFeb = reader.GetDecimal(i);
                        break;
                    case "CYMar":
                        CYMar = reader.GetDecimal(i);
                        break;
                    case "CYApr":
                        CYApr = reader.GetDecimal(i);
                        break;
                    case "CYMay":
                        CYMay = reader.GetDecimal(i);
                        break;
                    case "CYJun":
                        CYJun = reader.GetDecimal(i);
                        break;
                    case "CYJul":
                        CYJul = reader.GetDecimal(i);
                        break;
                    case "CYAug":
                        CYAug = reader.GetDecimal(i);
                        break;
                    case "CYSep":
                        CYSep = reader.GetDecimal(i);
                        break;
                    case "CYOct":
                        CYOct = reader.GetDecimal(i);
                        break;
                    case "CYNov":
                        CYNov = reader.GetDecimal(i);
                        break;
                    case "CYDec":
                        CYDec = reader.GetDecimal(i);
                        break;
                    case "NYJan":
                        NYJan = reader.GetDecimal(i);
                        break;
                    case "NYFeb":
                        NYFeb = reader.GetDecimal(i);
                        break;
                    case "NYMar":
                        NYMar = reader.GetDecimal(i);
                        break;
                    case "NYApr":
                        NYApr = reader.GetDecimal(i);
                        break;
                    case "NYMay":
                        NYMay = reader.GetDecimal(i);
                        break;
                    case "NYJun":
                        NYJun = reader.GetDecimal(i);
                        break;
                    case "NYJul":
                        NYJul = reader.GetDecimal(i);
                        break;
                    case "NYAug":
                        NYAug = reader.GetDecimal(i);
                        break;
                    case "NYSep":
                        NYSep = reader.GetDecimal(i);
                        break;
                    case "NYOct":
                        NYOct = reader.GetDecimal(i);
                        break;
                    case "NYNov":
                        NYNov = reader.GetDecimal(i);
                        break;
                    case "NYDec":
                        NYDec = reader.GetDecimal(i);
                        break;
                    case "ChangeDate":
                        ChangeDate = reader.GetDateTime(i);
                        break;
                    case "ChangeUser":
                        ChangeUser = reader.GetString(i);
                        break;
                    case "TokenVersion":
                        TokenVersion = reader.GetString(i);
                        break;
                    case "CEH1":
                        costElement.CEH1 = reader.GetString(i);
                        break;
                    case "CEH2":
                        costElement.CEH2 = reader.GetString(i);
                        break;
                    case "CEH3":
                        costElement.CEH3 = reader.GetString(i);
                        break;
                    case "CEH4":
                        costElement.CEH4 = reader.GetString(i);
                        break;
                    case "Description":
                        costElement.Description = reader.GetString(i);
                        break;
                    default:
                        break;
                }
            }
            CostElementMD = costElement;
        }
        public string CCCode { get; set; }
        public CostElementMD CostElementMD{ get; set; }
        public string Version { get; set; }
        public string Year { get; set; }
        public decimal PYJan { get; set; }
        public decimal PYFeb { get; set; }
        public decimal PYMar { get; set; }
        public decimal PYApr { get; set; }
        public decimal PYMay { get; set; }
        public decimal PYJun { get; set; }
        public decimal PYJul { get; set; }
        public decimal PYAug { get; set; }
        public decimal PYSep { get; set; }
        public decimal PYOct { get; set; }
        public decimal PYNov { get; set; }
        public decimal PYDec { get; set; }
        public decimal CYJan { get; set; }
        public decimal CYFeb { get; set; }
        public decimal CYMar { get; set; }
        public decimal CYApr { get; set; }
        public decimal CYMay { get; set; }
        public decimal CYJun { get; set; }
        public decimal CYJul { get; set; }
        public decimal CYAug { get; set; }
        public decimal CYSep { get; set; }
        public decimal CYOct { get; set; }
        public decimal CYNov { get; set; }
        public decimal CYDec { get; set; }
        public decimal NYJan { get; set; }
        public decimal NYFeb { get; set; }
        public decimal NYMar { get; set; }
        public decimal NYApr { get; set; }
        public decimal NYMay { get; set; }
        public decimal NYJun { get; set; }
        public decimal NYJul { get; set; }
        public decimal NYAug { get; set; }
        public decimal NYSep { get; set; }
        public decimal NYOct { get; set; }
        public decimal NYNov { get; set; }
        public decimal NYDec { get; set; }
        public DateTime ChangeDate { get; set; }
        public string ChangeUser { get; set; }
        public string TokenVersion { get; set; }
    }
}