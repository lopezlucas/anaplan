﻿using System;
using System.Data.SqlClient;

namespace ABWS.Models
{
    public class CostCenterMD
    {

        public CostCenterMD(){}

        /// <summary>
        /// Map the return data from the data base to object class.
        /// </summary>
        /// <param name="reader"></param>
        public CostCenterMD(SqlDataReader reader) {
            
            for(var i=0;i< reader.FieldCount; i++)
            {
                switch (reader.GetName(i))
                {
                    case "CC_CODE":
                        CCCode = reader.GetString(i);
                        break;
                    case "CC_Description":
                        CCDescription = reader.GetString(i);
                        break;
                    case "Status":
                        Status = reader.GetString(i);
                        break;
                    case "LastChangeDate":
                        LastChangeDate = reader.IsDBNull(i)?DateTime.MinValue:reader.GetDateTime(i);
                        break;
                    case "LastChangeUser":
                        LastChangeUser = reader.IsDBNull(i)?null:reader.GetString(i);
                        break;
                    default:
                        break;
                }
            }
        }

        //Cost Center Code
        public string CCCode { get; set; }
        public string CCDescription { get; set; }
        public string UserId { get; set; }
        public string Status { get; set; }
        public Nullable<DateTime> LastChangeDate { get; set; }
        public string LastChangeUser { get; set; }
        public string CCCodeAndDescription { get { return CCCode + " - " + CCDescription; } }

    }
}