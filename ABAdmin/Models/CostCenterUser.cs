﻿using System;
using System.Data.SqlClient;

namespace ABWS.Models
{
    public class CostCenterUser
    {

        public CostCenterUser(){}

        /// <summary>
        /// Map the return data from the data base to object class.
        /// </summary>
        /// <param name="reader"></param>
        public CostCenterUser(SqlDataReader reader) {
            
            for(var i=0;i< reader.FieldCount; i++)
            {
                switch (reader.GetName(i))
                {
                    case "CC_CODE":
                        CCCode = reader.GetString(i);
                        break;
                    case "UserID":
                        UserId = reader.GetString(i);
                        break;
                    default:
                        break;
                }
            }
        }

        //Cost Center Code
        public string CCCode { get; set; }
        public string UserId { get; set; }

    }
}