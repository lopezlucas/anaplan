﻿using ABWS.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace ABWS.DBAccess
{
    public class DBDataAccess
    {
        private string strConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;

        #region Financial Data

        /// <summary>
        /// Get version financial entries associated to a cost center.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="cc_code"></param>
        /// <returns></returns>
        public List<FinancialData> GetVersionFDByCCCode(string userId, string cc_code)
        {
            var financialDataLst = new List<FinancialData>();
            SqlConnection objSQLConnection = new SqlConnection(strConnectionString);
            try
            {
                SqlDataReader reader;
                SqlCommand objSQLCmd = new SqlCommand();

                objSQLCmd.CommandText = "spGetAccumulatedFinancialData";
                
                objSQLCmd.CommandType = CommandType.StoredProcedure;

                objSQLCmd.Parameters.Add("@UserId", SqlDbType.NVarChar, 8).Value = userId;
                objSQLCmd.Parameters.Add("@CC_Code", SqlDbType.NVarChar, 50).Value = cc_code;

                objSQLCmd.Connection = objSQLConnection;

                objSQLConnection.Open();
                reader = objSQLCmd.ExecuteReader();

                while (reader.Read())
                {
                    var financialData = new FinancialData(reader);

                    financialDataLst.Add(financialData);
                }

                return financialDataLst;
            }
            catch (Exception ex)
            {
                throw new Exception("Error in SQL:" + ex);
            }
            finally
            {
                objSQLConnection.Close();
            }
        }

        #endregion

        #region Cost Center

        /// <summary>
        /// Get the Cost Center data by User ID.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public List<CostCenterMD> GetCCByUserId(string userId)
        {
            var costCenterMDList = new List<CostCenterMD>();
            SqlConnection objSQLConnection = new SqlConnection(strConnectionString);

            try
            {
                SqlDataReader reader;
                SqlCommand objSQLCmd = new SqlCommand();

                objSQLCmd.CommandText = "spGetCostCenterMDByUserId";
                objSQLCmd.CommandType = CommandType.StoredProcedure;
                objSQLCmd.Parameters.Add("@UserId", SqlDbType.NVarChar, 8).Value = userId;

                objSQLCmd.Connection = objSQLConnection;

                objSQLConnection.Open();
                reader = objSQLCmd.ExecuteReader();

                while (reader.Read())
                {
                    var costCenterMD = new CostCenterMD(reader);

                    costCenterMDList.Add(costCenterMD);
                }

                return costCenterMDList;
            }
            catch (Exception ex)
            {
                throw new Exception("Error in SQL:" + ex);
            }
            finally
            {
                objSQLConnection.Close();
            }
        }

        /// <summary>
        /// Get the Cost Center Users.
        /// </summary>
        /// <returns></returns>
        public List<CostCenterUser> GetCCUsers()
        {
            var costCenterUserList = new List<CostCenterUser>();
            SqlConnection objSQLConnection = new SqlConnection(strConnectionString);

            try
            {
                SqlDataReader reader;
                SqlCommand objSQLCmd = new SqlCommand();

                objSQLCmd.CommandText = "spGetCostCenterUsers";
                objSQLCmd.CommandType = CommandType.StoredProcedure;

                objSQLCmd.Connection = objSQLConnection;

                objSQLConnection.Open();
                reader = objSQLCmd.ExecuteReader();

                while (reader.Read())
                {
                    var costCenterUser = new CostCenterUser(reader);

                    costCenterUserList.Add(costCenterUser);
                }

                return costCenterUserList;
            }
            catch (Exception ex)
            {
                throw new Exception("Error in SQL:" + ex);
            }
            finally
            {
                objSQLConnection.Close();
            }
        }

        /// <summary>
        /// Add Cost Center users.
        /// </summary>
        /// <param name="ccCode"></param>
        /// <param name="userId"></param>
        public void AddUsers(string ccCode,string userId)
        {
            SqlConnection objSQLConnection = new SqlConnection(strConnectionString);

            try
            {
                SqlCommand objSQLCmd = new SqlCommand();

                objSQLCmd.CommandText = "spAddCostCenterUsers";
                objSQLCmd.CommandType = CommandType.StoredProcedure;

                objSQLCmd.Parameters.Add("@CCCode", SqlDbType.NVarChar, 50).Value = ccCode;
                objSQLCmd.Parameters.Add("@UserId", SqlDbType.NVarChar, 8).Value = userId;

                objSQLCmd.Connection = objSQLConnection;

                objSQLConnection.Open();
                objSQLCmd.ExecuteNonQuery();
                
            }
            catch (Exception ex)
            {
                throw new Exception("Error in SQL:" + ex);
            }
            finally
            {
                objSQLConnection.Close();
            }
        }

        /// <summary>
        /// Delete Cost Center Users.
        /// </summary>
        /// <param name="ccCode"></param>
        /// <param name="userId"></param>
        public void DeleteUsers(string ccCode, string userId)
        {
            SqlConnection objSQLConnection = new SqlConnection(strConnectionString);

            try
            {
                SqlCommand objSQLCmd = new SqlCommand();

                objSQLCmd.CommandText = "spDeleteCostCenterUsers";
                objSQLCmd.CommandType = CommandType.StoredProcedure;

                objSQLCmd.Parameters.Add("@CCCode", SqlDbType.NVarChar, 50).Value = ccCode;
                objSQLCmd.Parameters.Add("@UserId", SqlDbType.NVarChar, 8).Value = userId;

                objSQLCmd.Connection = objSQLConnection;

                objSQLConnection.Open();
                objSQLCmd.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                throw new Exception("Error in SQL:" + ex);
            }
            finally
            {
                objSQLConnection.Close();
            }
        }

        /// <summary>
        /// Get the Cost Center Users.
        /// </summary>
        /// <returns></returns>
        public List<CostCenterMD> GetCostCenter()
        {
            var costCenterMDList = new List<CostCenterMD>();
            SqlConnection objSQLConnection = new SqlConnection(strConnectionString);

            try
            {
                SqlDataReader reader;
                SqlCommand objSQLCmd = new SqlCommand();

                objSQLCmd.CommandText = "spGetCostCenter";
                objSQLCmd.CommandType = CommandType.StoredProcedure;

                objSQLCmd.Connection = objSQLConnection;

                objSQLConnection.Open();
                reader = objSQLCmd.ExecuteReader();

                while (reader.Read())
                {
                    var costCenterMD = new CostCenterMD(reader);

                    costCenterMDList.Add(costCenterMD);
                }

                return costCenterMDList;
            }
            catch (Exception ex)
            {
                throw new Exception("Error in SQL:" + ex);
            }
            finally
            {
                objSQLConnection.Close();
            }
        }

        #endregion

        #region Cost Center User

        /// <summary>
        /// Check if exist Cost Center.
        /// </summary>
        /// <param name="cc_code"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public bool ExistCostCenterUser(string cc_code, string userId)
        {
            SqlConnection objSQLConnection = new SqlConnection(strConnectionString);
            var exist = false;

            try
            {
                SqlDataReader reader;
                SqlCommand objSQLCmd = new SqlCommand();

                objSQLCmd.CommandText = "spExistCostCenterUser";
                objSQLCmd.CommandType = CommandType.StoredProcedure;

                objSQLCmd.Parameters.Add("@CC_CODE", SqlDbType.NVarChar, 50).Value = cc_code;
                objSQLCmd.Parameters.Add("@UserId", SqlDbType.NVarChar, 8).Value = userId;

                objSQLCmd.Connection = objSQLConnection;

                objSQLConnection.Open();
                reader = objSQLCmd.ExecuteReader();

                while (reader.Read())
                {
                    //  If the value is greater than zero the user and code exist.
                    exist = reader.GetInt32(0) > 0;
                }

                return exist;
            }
            catch (Exception ex)
            {
                throw new Exception("Error in SQL:" + ex);
            }
            finally
            {
                objSQLConnection.Close();
            }
        }

        #endregion
    }
}