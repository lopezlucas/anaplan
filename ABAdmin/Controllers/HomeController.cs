﻿using ABWS.DBAccess;
using ABWS.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ABAdmin.Controllers
{
    public class HomeController : Controller
    {
        /// <summary>
        /// Show a grid with accumulated values of Financial Data.
        /// </summary>
        /// <param name="cc_code"></param>//Optional
        /// <returns></returns>
        public ActionResult Index(string cc_code = "")
        {
            var financialDtaLst = new List<FinancialData>();

            try
            {
                var db = new DBDataAccess();
                //  Get the system user or user of the web config.
                string userId = GetUserId(User);
                
                //  Get the Financial Data List by cost center code.
                if (!cc_code.Equals(""))
                    financialDtaLst = db.GetVersionFDByCCCode(userId, cc_code);
                //  Set the Cost Center list for the current user.
                @ViewBag.CostCenterList = db.GetCCByUserId(userId);
                //  Set the current Cost Center selected.
                @ViewBag.CostCenterSelected = cc_code;

            }catch(Exception ex)
            {
                return View("Error");
            }

            return View(financialDtaLst);
        }

        /// <summary>
        /// Show a list of Cost Center Users.
        /// </summary>
        /// <returns></returns>
        public ActionResult Users()
        {
            var costCenterUsersLst = new List<CostCenterUser>();

            try {
                var db = new DBDataAccess();

                //  Get the Cost Center list.
                costCenterUsersLst = db.GetCCUsers();
            }
            catch (Exception ex)
            {
                return View("Error");
            }
            return View(costCenterUsersLst);
        }

        /// <summary>
        /// Show the page for add users or insert.
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public ActionResult AddUsers(CostCenterUser user)
        {
            try
            {
                var db = new DBDataAccess();
                
                if (user.CCCode != null && user.UserId != null)
                {
                    if (!db.ExistCostCenterUser(user.CCCode, user.UserId))
                    {
                        db.AddUsers(user.CCCode, user.UserId);

                        return RedirectToAction("Users");
                    }
                    else
                    {
                        ViewBag.MsgDuplicateField = "The record already exists";
                    }
                }
                
                ViewBag.CostCenterList = db.GetCostCenter();
            }
            catch (Exception ex)
            {
                return View("Error");
            }

            return View();
        }

        /// <summary>
        /// Delete users and redirect to User page.
        /// </summary>
        /// <param name="ccCode"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public ActionResult DeleteUsers(string ccCode, string userId)
        {
            try
            {
                var db = new DBDataAccess();

                db.DeleteUsers(ccCode, userId);
            }
            catch (Exception ex)
            {
                return View("Error");
            }

            return RedirectToAction("Users");
        }

        /// <summary>
        /// Get the user ID. If not configured in the web.config get of the server.
        /// </summary>
        /// <param name="User"></param>
        /// <returns></returns>
        public static string GetUserId(System.Security.Principal.IPrincipal User)
        {
            var userId = GetConfiguration("userId");

            if (userId == "")
                userId = User.Identity.Name.Substring(5, 6);

            return userId;
        }

        /// <summary>
        /// Get the configurations into the web.config with the key.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string GetConfiguration(string key)
        {
            var configParameter = ConfigurationManager.AppSettings[key];

            return configParameter;
        }
    }
}